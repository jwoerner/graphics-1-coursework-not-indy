/*******************************************************************************
 *	Name:			DebugConsole
 *	Author:			Jordan Woerner
 *	Description:	DebugConsole allows the Game to print debugging details out 
					during runtime by redirecting the standard output pipes. 
					Requires _DEBUG to be defined for use.
 ******************************************************************************/
#ifndef DEBUG_CONSOLE_H
#define DEBUG_CONSOLE_H

#include <windows.h>
#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <io.h>

class DebugConsole
{
public:
	/*
	 DebugConsole()
	 Creates a default console with 500 rows and 80 columns.
	 */
	DebugConsole();
	/*
	 DebugConsole(int, int)
	 Creates a console with 'x' rows and 'y' columns.
	 */
	DebugConsole(int rows, int cols);

	/*
	 open()
	 Opens a console window. Redirecting standard output.
	 */
	void open();
	/*
	 close()
	 Closes a console window, removing the console.
	 */
	void close();

	/*
	 ~DebugConsole()
	 Removes the console.
	 */
	~DebugConsole();

private:
	const WORD MAX_ROWS;	// Maximum number of rows to keep in the buffer.
	const WORD MAX_COLS;	// Maximum number of characters per row.
	FILE* fp;
	HANDLE hStdHandle;
	int hConHandle;
};

#endif