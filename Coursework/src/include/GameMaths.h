/*******************************************************************************
 *	Name:			Game Maths
 *	Author:			Jordan Woerner
 *	Description:	Game Maths contains some useful functions for mathematics 
					inside the game, largely relating to vertex manipulation.
 ******************************************************************************/
#ifndef GAME_MATHS_H
#define GAME_MATHS_H

#include "Polyshape.h"
#include "BaseIncludes.h"
#include <math.h>

class GameMaths
{
public:	
	/*
	 static double crossProduct(const Vertex*, const Vertex*)
	 Returns the cross product of two provided vectors.
	 */
	static double crossProduct(const Vertex* v1, const Vertex* v2) {

		return ((v1->x * v2->y) - (v1->y * v2->x));
	}
	/*
	 static double dotProduct(const Vertex*. const Vertex*)
	 Returns the dot product of two vectors.
	 */
	static double dotProduct(const Vertex* v1, const Vertex* v2) {

		return ((v1->x * v2->x) + (v1->y * v2->y));
	}
	/*
	 static double length(const Vertex*)
	 Returns the length of a provided vector.
	 */
	static double length(const Vertex* v) {

		return sqrt((v->x * v->x) + (v->y * v->y));
	}

};

#endif