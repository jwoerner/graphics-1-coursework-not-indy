#include "include\DPlatform.h"

DPlatform::DPlatform() {

	solid = false;
}

DPlatform::DPlatform(double x, double y, double w, double h, double r) 
	: Platform(x, y, w, h, r) {

	solid = false;
}

std::wistream& operator>>(std::wistream& in, DPlatform& p) {

	double w, h, x, y;
	wchar_t c1, c2, c3;

	if (in >> x >> c1 >> y >> c2 >> w >> c3 >> h) {
		if (c1 == ',' && c2 == ',' && c3 == ',') {
			p.setXPos(x);
			p.setYPos(y);
			p.setWidth(w);
			p.setHeight(h);
			p.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "Platform input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const DPlatform& p) {

	out << "dplat(" << p.getXPos() << ", " << p.getYPos();
	out << ", " << p.getWidth() << ", " << p.getHeight() << ")";
	return out;
}