/*******************************************************************************
 *	Name:			Game
 *	Author:			Jordan Woerner
 *	Description:	Game is the core component of my game. It contains the 
					pointer to the player of the game, the world the player will
					 interact with, and various other aspects of the game.
					Allowing them all to interact.
 ******************************************************************************/
#ifndef GAME_H
#define GAME_H

#include "EventHandler.h"
#include "LevelBox.h"
#include "Camera.h"
#include "MovingPlatform.h"
#include "Player.h"
#include "FreeType.h"
#include "Texture.h"
#include "MenuBackground.h"

#include <string>
#include <sstream>
#include <vector>

class Game
{
public:
	/*
	 Game(EventHandler&)
	 Creates a new Game and hooks it to the specified EventHandler.
	 */
	Game(EventHandler& handler);
	/*
	 initMainMenu()
	 Creates and switches to the main menu.
	 */
	void initMainMenu();
	/*
	 initLevelMenu()
	 Creates and switches to the level select menu.
	 */
	void initLevelMenu();
	/*
	 populateLevelMenu()
	 Populates the level menu with all currently stored levels.
	 */
	void populateLevelMenu();
	/*
	 startGame()
	 Starts the game by loading the world and setting up entities.
	 */
	void startGame(std::wstring lvlName);
	/*
	 initWinScreen()
	 Sets up the win screen.
	 */
	void initWinScreen();
	/*
	 initLoseScreen()
	 Sets up the lose screen.
	 */
	void initLoseScreen();

	/*
	 bool isFirstUpdate()
	 Returns true if this is the first update for the Playing state of the game.
	 */
	inline bool isFirstUpdate() {

		return firstUpdate;
	}
	/*
	 attatchCam(Camera*)
	 Adds a Camera for this Game to use.
	 */
	void attatchCam(Camera* c);
	/*
	 checkCollisions()
	 Checks the entities in this Game for collisions.
	 */
	void checkCollisions();
	/*
	 update(double)
	 Updates every entity in the game. Accepts a double as a smoothing value.
	 */
	void update(double deltaT);
	/*
	 display()
	 Displays every entity in the game.
	 */
	void display();

private:
	EventHandler& ev;	// The EventHandler this Game will use.
	GameState oldState;	// The current state of the game.

	freetype::font_data uiFont;			// A FreeType font to render large UI.
	freetype::font_data uiFontSmall;	// A FreeType font to render small UI.
	Vertex colVec;
	bool colliding;		// Is something colliding?
	bool firstUpdate;	// Is this the first update of the game?
	bool keyDown;		// Checks if a key is down currnetly.
	GLushort boundTex;	// What is the currently bound texture?
	int seconds;		// The number of elapsed seconds.
	clock_t pauseStart;	// The time that the game paused at.

	MenuBackground bg;	// A menu background.
	GLuint hud;

	Camera cam;						// The Camera for this game.
	std::shared_ptr<Player> ply;	// The Player for this Game.
	std::shared_ptr<GameGoal> goal;	// The goal for the loaded level.
	std::vector<std::shared_ptr<StaticEntity>> ents;	// A list of entities.
	std::vector<std::shared_ptr<StaticEntity>> solids;	// A list of entities.
	std::vector<LevelBox> levels;	// A list of levels.

	/*
	 bool loadLevel(char*)
	 Loads a level using the provided char array as a name.
	 */
	bool loadLevel(std::wstring lvlName);
	/*
	 cleanUpGL()
	 Cleans up any OpenGL resources that don't need to exist anymore.
	 */
	void cleanUpGL() {
		for(std::vector<std::shared_ptr<StaticEntity>>::iterator it = ents.begin(); 
			it != ents.end(); 
			++it) {
				const GLuint t = (*it)->getTexture();
				glDeleteTextures(1, &t);
		}
		for(std::vector<LevelBox>::iterator it = levels.begin(); 
			it != levels.end(); 
			++it) {
				const GLuint t = (it)->getTexture();
				glDeleteTextures(1, &t);
		}
	}

	/*
	 TextureCmp
	 A comparator for entities to ensure they are ordered by texture for optimal
	  rendering.
	 */
	typedef struct TextureCmp {
		bool operator()(std::shared_ptr<StaticEntity>& s1, std::shared_ptr<StaticEntity>& s2) const {

			if (s1->getLayer() > s2->getLayer()) {
				return true;
			} else if (s1->getLayer() == s2->getLayer()) {
				return s1->getTexture() > s2->getTexture();
			}

			return false;
		}
	};
};

#endif