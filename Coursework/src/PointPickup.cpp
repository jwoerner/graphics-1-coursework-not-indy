#include "include\PointPickup.h"
#include "include\BaseIncludes.h"
#include "include\Player.h"

PointPickup::PointPickup() {

	active = true;
	solid = true;
	points = 0;
	layer = SCENERY_F;
}

PointPickup::PointPickup(int pts) {

	active = true;
	solid = true;
	points = pts;
	layer = SCENERY_F;
}

void PointPickup::draw() {

	if (active) {
		u = 1.0;
		v = 1.0;
		StaticEntity::draw();
	}
}

std::wistream& operator>>(std::wistream& in, PointPickup& p) {

	double w, h, x, y;
	int pt;
	wchar_t delim = ',';
	wchar_t c1, c2, c3, c4;

	if (in >> x >> c1 >> y >> c2 >> w >> c3 >> h >> c4 >> pt) {
		if (c1 == ',' && c2 == ',' && c3 == ',' && c4 == ',') {
			p.setXPos(x);
			p.setYPos(y);
			p.setWidth(w);
			p.setHeight(h);
			p.setPoints(pt);
			p.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "PointPickup input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const PointPickup& p) {

	out << "PointPickup(" << p.getXPos() << ", " << p.getYPos();
	out << ", " << p.getWidth() << ", " << p.getHeight() << p.getPoints() << ")";
	return out;
}

void PointPickup::respond(Player& ent, Vertex& mtd) {

	if (active) {
		ent.modifyPoints(points);
		active = false;
	}
}