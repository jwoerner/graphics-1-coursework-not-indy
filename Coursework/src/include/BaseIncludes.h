/*******************************************************************************
 *	Name:			BaseIncludes
 *	Author:			Jordan Woerner
 *	Description:	A set of includes that will be used by a large number of 
					classes.
 ******************************************************************************/
#ifndef BASE_INCLUDES_H
#define BASE_INCLUDES_H

/*****************************************************************
 * Vital includes for the game. Add commonly used externals here.
 *****************************************************************/
#include <Windows.h>
#include <iostream>
#include <gl\GL.h>
#include <gl\GLU.h>

/*****************************************************************
 * Macro definitions for various settings in the game go here.
 *****************************************************************/
/* Uncomment this if you want to see the collision meshes and states. */
//#define SHOW_COLLISIONS

#endif