#include "include\BaseIncludes.h"
#include "include\Platform.h"

Platform::Platform() {

	layer = NORMAL;
	solid = true;
}

Platform::Platform(double x, double y, double w, double h, double r) 
	: StaticEntity(x, y, w, h, r) {

		layer = NORMAL;
		solid = true;
}

std::wistream& operator>>(std::wistream& in, Platform& p) {

	double w, h, x, y;
	wchar_t c1, c2, c3;

	if (in >> x >> c1 >> y >> c2 >> w >> c3 >> h) {
		if (c1 == ',' && c2 == ',' && c3 == ',') {
			p.setXPos(x);
			p.setYPos(y);
			p.setWidth(w);
			p.setHeight(h);
			p.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "Platform input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const Platform& p) {

	out << "plat(" << p.getXPos() << ", " << p.getYPos();
	out << ", " << p.getWidth() << ", " << p.getHeight() << ")";
	return out;
}