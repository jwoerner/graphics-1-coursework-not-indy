/*******************************************************************************
 *	Name:			PointPickup
 *	Author:			Jordan Woerner
 *	Description:	A set of includes that will be used by a large number of 
					classes.
 ******************************************************************************/
#ifndef POINT_PICKUP_H
#define POINT_PICKUP_H

#include "StaticEntity.h"

class PointPickup :	public StaticEntity
{
private:
	bool active;	// Is this PointPickup active?
	int points;		// The value of this PointPickup.

public:
	/*
	 PointPickup()
	 Creates a PointPickup.
	 */
	PointPickup();
	/*
	 PointPickup(int)
	 Creates a PointPickup worth the specified amount.
	 */
	PointPickup(int pts);

	/*
	 setPoints(int)
	 Sets the value of this PointPickup.
	 */
	inline void setPoints(int p) { points = p; }
	/*
	 int getPoints() const
	 Gets the value of this PointPickup.
	 */
	inline int getPoints() const { return points; }
	/*
	 setActive(bool)
	 Sets whether this PointPickup is acive or not.
	 */
	inline void setActive(bool a) { active = a; }
	/*
	 bool isActive() const
	 Returns true if this PointPickup is active or not.
	 */
	inline bool isActive() const { return active; }

	void draw();

	void collide(StaticEntity& ent, Vertex& mtd) { if(active) ent.respond(*this, mtd); }
	/*
	 Collision handlers.
	 */
	void respond(Player& ent, Vertex& mtd);

};

std::wistream& operator>>(std::wistream& in, PointPickup& p);
std::ostream& operator<<(std::ostream& out, const PointPickup& p);

#endif