/*******************************************************************************
 *	Name:			Polyshape
 *	Author:			Jordan Woerner
 *	Description:	Polyshape defines the shape of an drawable or collision 
					entity. Contains two arrays of vertices, vert is the 
					render area, cVert is the collision box.
 ******************************************************************************/
#ifndef POLYHSAPE_H
#define POLYHSAPE_H

/*******************************************************************************
A vertex holds X and Y positions, along with U/V texture coordinates.
*******************************************************************************/
class Vertex {
public:
	Vertex();
	double x, y;

};

/* Operator overloads for Vertex. */
bool operator==(const Vertex& first, const Vertex& other);
inline bool operator!=(const Vertex& first, const Vertex& other) { return !operator==(first, other); }

bool operator==(const Vertex& first, const double& other);
inline bool operator!=(const Vertex& first, const double& other) { return !operator==(first, other); }

bool operator>(const Vertex& first, const Vertex& other);
inline bool operator<(const Vertex& first, const Vertex& other) { return operator>(other, first); }
inline bool operator>=(const Vertex& first, const Vertex& other) { return !operator<(first, other); }
inline bool operator<=(const Vertex& first, const Vertex& other) {return !operator>(first, other); }

bool operator>(const Vertex& first, const double& other);
inline bool operator>(const double& first, const Vertex& other) { return operator>(other, first); }
inline bool operator<(const Vertex& first, const double& other) { return operator>(other, first); }
inline bool operator>=(const Vertex& first, const double& other) { return !operator<(first, other); }
inline bool operator<=(const Vertex& first, const double& other) {return !operator>(first, other); }

class Polyshape {

public:
	Polyshape(int num);

	Vertex *vert;	// Rendering vertices.
	Vertex *cVert;	// Collision verticies.
	int numVerts;	// The number of verticies.
	Vertex center;	// The center of the collision verticies.
};

#endif