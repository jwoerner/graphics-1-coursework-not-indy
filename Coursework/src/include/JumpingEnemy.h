/*******************************************************************************
 *	Name:			JumpingEnemy
 *	Author:			Jordan Woerner
 *	Description:	JumpingEnemy is an enemy that jumps up after a certain 
					amount of time (specified by jumps per minute), to a 
					specified height.
 ******************************************************************************/
#ifndef JUMPING_ENEMY_H
#define JUMPING_ENEMY_H

#include "BaseEnemy.h"
#include <time.h>

class JumpingEnemy : public BaseEnemy
{
private:
	unsigned int jumpsPerMin;	// How many times to jump per minute.
	clock_t sClock;				// The time since the last jump.

public:
	/*
	 JumpingEnemy()
	 Creates a new default JumpingEnemy.
	 */
	JumpingEnemy();
	/*
	 JumpingEnemy(unsigned char, char, double, unsigned int)
	 Creates a new JumpingEnemy with the provided health, doing the specified 
		damage, that jumps to the specified height at the specified times per 
		minute.
	 */
	JumpingEnemy(unsigned char hp, char d, double jH, unsigned int jPM);
	/*
	 JumpingEnemy(double, double, double, double, unsigned char, char, double, 
		unsigned int)
	 Creates a jumping enemy positioned as specified.
	 */
	JumpingEnemy(double x, double y, double w, double h, unsigned char hp, char d, double jH, unsigned int jPM);

	/*
	 setJumpsPerMinute(unsigned int)
	 Sets the number of times this enemy should jump each minute.
	 */
	inline void setJumpsPerMinute(unsigned int num) { jumpsPerMin = num; }
	/*
	 unsigned int getJumpsPerMinute() const
	 Gets the number of times this enemy jumps per minute.
	 */
	inline unsigned int getJumpsPerMinute() const { return jumpsPerMin; }

	void update(double deltaT);

	inline void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }

	void respond(Platform& ent, Vertex& mtd);
};

std::wistream& operator>>(std::wistream& oStr, JumpingEnemy& j);
std::ostream& operator<<(std::ostream& oStr, const JumpingEnemy& j);

#endif