/*******************************************************************************
 *	Name:			WalkingEnemy
 *	Author:			Jordan Woerner
 *	Description:	WalkingEnemy is a enemy that walks left and right for a 
					specified distance at a specified speed or until it hits 
					something before turning around.
 ******************************************************************************/
#ifndef WALKING_ENEMY_H
#define WALKING_ENEMY_H

#include "BaseEnemy.h"

class WalkingEnemy : public BaseEnemy
{
private:
	double walkDist;	// The distance this enemy should walk.
	bool walkLeft;		// Is the enemy walking left?

public:
	/*
	 WalkingEnemy()
	 Creates a WalkingEnemy with no distance or speed facing right.
	 */
	WalkingEnemy();
	/*
	 WalkingEnemy(double, double, bool)
	 Creates a walking enemy with the specified speed and walk distance, can be set to face left.
	 */
	WalkingEnemy(double spd, double dist, bool left);
	/*
	 WalkingEnemy(double, double, double, double, unsigned char, char, double, double, bool)
	 Creates a walking enemy at the specified location, with the set width and height, health, damage speed and distance.
	 */
	WalkingEnemy(double x, double y, double w, double h, unsigned char hp, char dam, double spd, double dist, bool left);

	/*
	 setWalkDistance(double)
	 Sets the walking distance for this enemy.
	 */
	inline void setWalkDistance(double d) { walkDist = d; }
	/*
	 double getWalkDistance() const
	 Gets the walking distance of this enemy.
	 */
	inline double getWalkDistance() const { return walkDist; }
	/*
	 setWalkLeft(bool)
	 Sets if this enemy should walk left or right.
	 */
	inline void setWalkLeft(bool left) { walkLeft = left; }
	/*
	 bool isWalkingLeft() const
	 Returns true if the enemy is facing left, false if the enemy is facing right.
	 */
	inline bool isWalkingleft() const { return walkLeft; }

	/*
	 update(double)
	 Updates this enemy.
	 */
	void update(double deltaT);

	/*
	 Collision responder.
	 */
	inline void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }
	/*
	 Collision response.
	 */
	void respond(Platform& ent, Vertex& mtd);
};

std::wistream& operator>>(std::wistream& oStr, WalkingEnemy& w);
std::ostream& operator<<(std::ostream& oStr, const WalkingEnemy& w);

#endif