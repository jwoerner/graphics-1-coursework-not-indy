/*******************************************************************************
 *	Name:			Spike
 *	Author:			Jordan Woerner
 *	Description:	Spikes are static entities that damage anything that touches them.
 ******************************************************************************/
#ifndef SPIKE_H
#define SPIKE_H

#include "StaticEntity.h"

class Spike : public StaticEntity
{
private:
	short damage;	// The damage the spike does.

public:
	/*
	 Spike()
	 Creates a default Spike entity.
	 */
	Spike();
	/*
	 Spike(double, double, double, double, short)
	 Creates a Spike at the specified x/y with the specified width, height and damage.
	 */
	Spike(double x, double y, double w, double h, short dam);

	/*
	 setDamage(short)
	 Sets the damage of this Spike.
	 */
	inline void setDamage(short d) { damage = d; }
	/*
	 short getDamage() const
	 Gets the damage this Spike does.
	 */
	inline short getDamage() const { return damage; }

	void updateVerts();

	void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }
	/*
	 Collision handler.
	 */	
	void respond(Player& ply, Vertex& mtd);
};

std::wistream& operator>>(std::wistream& in, Spike& s);
std::ostream& operator<<(std::ostream& out, const Spike& s);

#endif