/*******************************************************************************
 *	Name:			DPlatform
 *	Author:			Jordan Woerner
 *	Description:	DPlatforms are a combination of Platforms and Decorations, 
					allowing for decorations that scale their textures rather 
					than stretching.
 ******************************************************************************/
#ifndef D_PLATFORM_H
#define D_PLATFORM_H

#include "Platform.h"

class DPlatform : public Platform
{
public:
	/*
	 DPlatform()
	 Creates a DPlatform at zero x/y with 1.0 width/ height and no rotation.
	 */
	DPlatform();
	/*
	 DPlatform(double, double, double, double, double)
	 Creates a DPlatform at the speciied x/y with the provided width, height and rotation.
	 */
	DPlatform(double x, double y, double w, double h, double r);
};

std::wistream& operator>>(std::wistream& in, DPlatform& p);
std::ostream& operator<<(std::ostream& out, const DPlatform& p);

#endif