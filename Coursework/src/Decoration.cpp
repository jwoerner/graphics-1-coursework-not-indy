#include "include\Decoration.h"

Decoration::Decoration() {

	solid = false;
	layer = SCENERY_B;
}

Decoration::Decoration(double x, double y, double w, double h, double r) 
	: StaticEntity(x, y, w, h, r) {

	solid = false;
	layer = SCENERY_B;
}

void Decoration::setTexture(GLuint tex) {

	texID = tex;
	s = 0.0f;
	t = 0.0f;
	u = 1.0f;
	v = 1.0f;
}

std::wistream& operator >>(std::wistream& in, Decoration& d) {
	
	double w, h, x, y;
	wchar_t c1, c2, c3;

	if (in >> x >> c1 >> y >> c2 >> w >> c3 >> h) {
		if (c1 == ',' && c2 == ',' && c3 == ',') {
			d.setXPos(x);
			d.setYPos(y);
			d.setWidth(w);
			d.setHeight(h);
			d.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "Decoration input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

std::ostream& operator <<(std::ostream& out, const Decoration& d) {

	out << "decor(" << d.getXPos() << ", " << d.getYPos() << ", ";
	out <<d.getWidth() << ", " << d.getHeight() << ")";
	return out;
}