/*******************************************************************************
 *	Name:			BaseEnemy
 *	Author:			Jordan Woerner
 *	Description:	A base for enemies to draw from. Contains all the major 
					shared attributes of enemies such as health and damage.
 ******************************************************************************/
#ifndef BASE_ENEMY_H
#define BASE_ENEMY_H

#include "MovingEntity.h"

class BaseEnemy : public MovingEntity
{
protected:
	double startXPos;		// The starting X position of this enemy.
	double startYPos;		// The starting Y position of this enemy.
	unsigned char health;	// The health of this enemy.
	char damage;			// The damage this enemy does to others.

public:
	/*
	 BaseEnemy()
	 Creates a new BaseEnemy with default values.
	 */
	BaseEnemy();
	/*
	 BaseEnemy(double, double, double, double, unsigned char, char)
	 Creates a new BaseEnemy at the specified x/y with the specified width, 
		height, health and damage.
	 */
	BaseEnemy(double x, double y, double w, double h, unsigned char hp, char d);

	/*
	 setHealth(unsigned char)
	 Sets the health of this enemy.
	 */
	inline void setHealth(unsigned char h) { health = h; }
	/*
	 modifyHealth(char)
	 Adjusts the health of this enemy by the provided amount.
	 */
	inline void modifyHealth(char h) { health = h; }
	/*
	 unsigned char getHealth() const
	 Gets the health of this enemy.
	 */
	inline unsigned char getHealth() const { return health; }
	/*
	 setDamage(char)
	 Sets the damage this enemy does.
	 */
	inline void setDamage(char d) { damage = d; }
	/*
	 char getDamage() const
	 Gets the damage this enemy does to others.
	 */
	inline char getDamage() const { return damage; }

	void setTexture(GLuint t);
};

#endif