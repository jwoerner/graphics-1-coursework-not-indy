#include "include\JumpingEnemy.h"

JumpingEnemy::JumpingEnemy() {

	ySpeed = 0.0;
	jumpsPerMin = 0;
	sClock = clock();
}

JumpingEnemy::JumpingEnemy(unsigned char hp, char d, double jH, unsigned int jPM)
	: BaseEnemy(0.0, 0.0, 1.0, 1.0, hp, d) {

		ySpeed = jH;
		jumpsPerMin = jPM;
		sClock = clock();
}

JumpingEnemy::JumpingEnemy(double x, double y, double w, double h, unsigned char hp, char d, double jH, unsigned int jPM) 
	: BaseEnemy(x, y, w, h, hp, d) {

		ySpeed = jH;
		jumpsPerMin = jPM;
		sClock = clock();
}

void JumpingEnemy::update(double deltaT) {

	MovingEntity::update(deltaT);

	if ((double)((clock() - sClock)/CLOCKS_PER_SEC) >= (60/jumpsPerMin)) {
		/* Once the jumps per second time has passed, the enemy should jump. */
		velocityY = ySpeed;
		sClock = clock();
		t = 0.0f;
		v = 0.5f;
	}
}

std::wistream& operator>>(std::wistream& iStr, JumpingEnemy& j) {

	double w, h, x, y, jH;
	unsigned int hp, d, jPM;
	wchar_t c1, c2, c3, c4, c5, c6, c7;

	if (iStr >> x >> c1 >> y >> c2 >> w >> c3 >> h >> c4 >> hp >> c5 >> d >> c6 >> jH >> c7 >> jPM) {
		if (c1 == ',' && c2 == ',' && c3 == ',' && c4 == ',' 
			&& c5 == ',' && c6 == ',' && c7 == ',') {
				j.setXPos(x);
				j.setYPos(y);
				j.setWidth(w);
				j.setHeight(h);
				j.setHealth(hp);
				j.setDamage(d);
				j.setYSpeed(jH);
				j.setJumpsPerMinute(jPM);
				j.updateVerts();
		} else {
			iStr.setstate(std::ios_base::failbit);
			std::cerr << "JumpingEnemy input not properly formed!" 
				<< std::endl;
		}
	}

	return iStr;
}

std::ostream& operator<<(std::ostream& oStr, const JumpingEnemy& j) {

	oStr << "jumpen(" << j.getXPos() << ", " << j.getYPos() << ", " << j.getWidth() << ", " << j.getHeight();
	oStr << ", " << (int)j.getHealth() << ", " << (int)j.getDamage();
	oStr << ", " << j.getYSpeed() << ", " << j.getJumpsPerMinute() << ")";

	return oStr;
}

void JumpingEnemy::respond(Platform& ent, Vertex& mtd) {

	xPos += mtd.x;
	yPos += mtd.y;
	t = 0.5f;
	v = 1.0f;
}