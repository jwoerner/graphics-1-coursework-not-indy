#include "include\StaticEntity.h"
#include "include\Matrix.h"

#define xWidth (StaticEntity::xPos + StaticEntity::width)
#define yHeight (StaticEntity::yPos + StaticEntity::height)

StaticEntity::StaticEntity() : verts(4)
{
	xPos = 0.0;
	yPos = 0.0;
	rot = 0.0;
	layer = NORMAL;
	width = 1.0;
	height = 1.0;
	s = 0.0f;
	t = 0.0f;
	u = 1.0f;
	v = 1.0f;
	colliding = false;
	solid = false;
	updateVerts();
}

StaticEntity::StaticEntity(double x, double y, double w, double h, double r) : verts(4) {

	xPos = x;
	yPos = y;
	rot = r;
	layer = NORMAL;
	width = w;
	height = h;
	s = 0.0f;
	t = 0.0f;
	u = 1.0f;
	v = 1.0f;
	colliding = false;
	solid = false;
	updateVerts();
}

void StaticEntity::updateVerts() {

	/* Horrible block of assignment. */
	/* Collision box = ((Render Area	 = n)	 - offset) */
	verts.vert[0].x = ((verts.cVert[0].x = xPos) - C_OFFSET);
	verts.vert[0].y = ((verts.cVert[0].y = yPos) - C_OFFSET);
	verts.vert[1].x = ((verts.cVert[1].x = xWidth) + C_OFFSET);
	verts.vert[1].y = ((verts.cVert[1].y = yPos) - C_OFFSET);
	verts.vert[2].x = ((verts.cVert[2].x = xWidth) + C_OFFSET);
	verts.vert[2].y = ((verts.cVert[2].y = yHeight) + C_OFFSET);
	verts.vert[3].x = ((verts.cVert[3].x = xPos) - C_OFFSET);
	verts.vert[3].y = ((verts.cVert[3].y = yHeight) + C_OFFSET);

	verts.center.x = (verts.cVert[0].x + verts.cVert[2].x)/2;
	verts.center.y = (verts.cVert[0].y + verts.cVert[2].y)/2;
}

void StaticEntity::setTexture(GLuint tex, int w, int h) {

	texID = tex;
	s = 0.0f;
	t = 0.0f;
	u = (float)width * (64/(float)w);
	v = (float)height * (64/(float)h);
}

void StaticEntity::draw() { 

	glPushMatrix();
		/* Only enable textures if there is one. */
		if (texID != 0) {
			glEnable(GL_TEXTURE_2D);
		}
		
		glBegin(GL_TRIANGLE_STRIP);
			glColor3d(1.0, 1.0, 1.0);
			glTexCoord2f(s, t);
			glVertex3d(verts.vert[0].x, verts.vert[0].y, layer);
			glTexCoord2f(s, v);
			glVertex3d(verts.vert[3].x, verts.vert[3].y, layer);
			glTexCoord2f(u, t);
			glVertex3d(verts.vert[1].x, verts.vert[1].y, layer);
			glTexCoord2f(u, v);
			glVertex3d(verts.vert[2].x, verts.vert[2].y, layer);
		glEnd();

		if (texID != 0) {
			glDisable(GL_TEXTURE_2D);
		}
	glPopMatrix();

	#ifdef SHOW_COLLISIONS
	if (solid) {
		glPushMatrix();
			glLineWidth(2.0);
			glLineStipple(1, 1);
			glColor3d(0.0, 0.0, 1.0);
			glBegin(GL_LINE_LOOP);
				glVertex3d(verts.cVert[3].x, verts.cVert[3].y, 0.9);
				glVertex3d(verts.cVert[0].x, verts.cVert[0].y, 0.9);
				glVertex3d(verts.cVert[1].x, verts.cVert[1].y, 0.9);
				glVertex3d(verts.cVert[2].x, verts.cVert[2].y, 0.9);
			glEnd();
			if (colliding) {
				glColor3d(0.0, 1.0, 0.0);
			} else {
				glColor3d(1.0, 0.0, 0.0);
			}
			glPointSize(4.0f);
			glBegin(GL_POINTS);
				glVertex3d(verts.cVert[3].x, verts.cVert[3].y, 1.0);
				glVertex3d(verts.cVert[0].x, verts.cVert[0].y, 1.0);
				glVertex3d(verts.cVert[1].x, verts.cVert[1].y, 1.0);
				glVertex3d(verts.cVert[2].x, verts.cVert[2].y, 1.0);
				glColor3d(0.0, 1.0, 1.0);
				glVertex3d(verts.center.x, verts.center.y, 1.0);
			glEnd();
		glPopMatrix();
	}
	#endif
}