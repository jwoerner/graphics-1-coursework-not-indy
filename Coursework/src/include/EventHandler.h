/*******************************************************************************
 *	Name:			EventHandler
 *	Author:			Jordan Woerner
 *	Description:	EventHandler holds the state of various inputs from the 
					system, along with storing other input related things for 
					game logic. The class is intended for a singular use per 
					application, to then be shared across other objects allowing
					 for a centralised object to hold all input data.
 ******************************************************************************/
#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

/*
 GameState
 An enumeration of all possible states the game can be in.
 */
enum GameState {

	MAIN_MENU,
	LEVEL_MENU,
	PLAYING,
	PAUSE,
	GAME_OVER,
	HI_SCORES
};

class EventHandler {

private:
	bool keys[256];		// An array of the states for 256 keys.
	int mouseX;			// The X position of the mouse.
	int mouseY;			// The Y position of the mouse.
	bool mouseLeft;		// The left mouse button state.
	bool mouseRight;	// The right mouse button state.
	GameState state;	// The current game state.

public:
	/*
	 EventHandler()
	 Creates a new EventHandler to manage the key presses and mouse movements 
	 between objects in the game.
	 */
	EventHandler();

	/*
	 setKey(unsigned char, bool)
	 Sets the specified key to either true or false depending on the state of 
	 the key.
	 */
	inline void setKey(unsigned char key, bool val) {

		keys[key] = val;
	}
	/*
	 bool getKey(unsigned char)
	 Returns the state of the specified key, true for down, false for up.
	 */
	inline bool getKey(unsigned char key) const {

		return keys[key];
	}
	/*
	 setMouseX(unsigned int)
	 Sets the stored X position of the mouse cursor.
	 */
	inline void setMouseX(unsigned int x) {

		mouseX = x;
	}
	/*
	 unsigned int getMouseX()
	 Returns the X position of the mouse;
	 */
	inline unsigned int getMouseX() const {

		return mouseX;
	}
	/*
	 setMouseY(unsigned  int y)
	 Sets the stored Y position of the mouse cursor.
	 */
	inline void setMouseY(unsigned int y) {

		mouseY = y;
	}
	/*
	 unsigned int getMouseY()
	 Returns the Y position of the mouse;
	 */
	inline unsigned int getMouseY() const {

		return mouseY;
	}
	/*
	 setMouseLeft(bool)
	 Sets the state of the left mouse button, true for down, false for up.
	 */
	inline void setMouseLeft(bool down) {

		mouseLeft = down;
	}
	/*
	 bool getMouseLeft()
	 Returns the state of the left mouse button. True for down, false for up.
	 */
	inline bool getMouseLeft() const {

		return mouseLeft;
	}
	/*
	 setMouseRight(bool)
	 Sets the state of the right mouse button, true for down, false for up.
	 */
	inline void setMouseRight(bool down) {

		mouseRight = down;
	}
	/*
	 bool getMouseRight()
	 Returns the state of the right mouse button. True for down, false for up.
	 */
	inline bool getMouseRight() const {

		return mouseRight;
	}

	/*
	 void setGameState(GameState)
	 Sets the current GameState to the specified enumeration value.
	 */
	inline void setGameState(GameState s) {

		state = s;
	}
	/*
	 GameState getGameState()
	 Gets the current state of the game as a GameState enumeration value.
	 */
	inline GameState getGameState() const {

		return state;
	}

	/*
	 reset()
	 Resets this EventHandler.
	 */
	inline void reset() {

		for (int i = 0; i < 256; i++) {
			EventHandler::keys[i] = false;
		}
		EventHandler::mouseLeft = false;
		EventHandler::mouseRight = false;
		EventHandler::mouseX = 0;
		EventHandler::mouseY = 0;
	}

};

#endif