#include "include\BaseIncludes.h"
#include "include\Player.h"
#include "include\Platform.h"
#include "include\MovingPlatform.h"
#include "include\JumpingEnemy.h"
#include "include\WalkingEnemy.h"
#include "include\GameGoal.h"

#define xWidth (StaticEntity::xPos + StaticEntity::width)
#define yHeight (StaticEntity::yPos + StaticEntity::height)

Player::Player(EventHandler& handler) 
	: ev(handler), MovingEntity(0.0, 0.0, 1.5, 4.0, 0.0, 2.5, 2.5) {

	solid = true;
	applyFriction = true;
	applyGravity = true;

	health = 5;
	maxHealth = 5;
	lives = 3;
	totalPoints = 0;
	invuln = false;

	aniOffset = 0.25f;
	s = 0.0;
	u = 0.25;
	faceLeft = false;
	aniRev = false;
	frameRate = 4;
	frame = 1;
	frames = 1;
	curFrame = clock();
}

Player::Player(EventHandler& handler, int h, int l) 
	: ev(handler), MovingEntity(0.0, 0.0, 1.5, 4.0, 0.0, 2.5, 2.5) {

	solid = true;
	applyFriction = true;
	applyGravity = true;

	health = h;
	maxHealth = h;
	lives = l;
	totalPoints = 0;
	invuln = false;

	aniOffset = 0.25f;
	s = 0.0;
	u = 0.25;
	faceLeft = false;
	aniRev = false;
	frameRate = 4;
	frame = 1;
	frames = 1;
	curFrame = clock();
}

void Player::update(double deltaT) {

	MovingEntity::update(deltaT); // Update the position of the player.
	handleInput(deltaT); // Get the player input.
	clock_t thisClock = clock(); // Get the clock for this cycle (saves multiple calls).

	/* The player has stopped moving. */
	moveDir = MV_DOWN;

	/* Select the correct animation based on x velocity. */
	if ((ev.getKey(0x41) || ev.getKey(VK_LEFT)) 
		|| (ev.getKey(0x44) || ev.getKey(VK_RIGHT))) {
			/* The player is moving either left or right. */
			moveDir = MV_RIGHT;
	}
	
	if (!colliding && (fabs(velocityY) > 0.05)) {
		/* If the player isn't colliding, and they are falling, set the animation. */
		moveDir = MV_UP;
	}

	/* Kill the player if their health drops to zero. */
	if (health <= 0)
		kill();

	/* If the Player is invulnerable, we need to stop it after 3 seconds. */
	if (invuln) {
		if (((thisClock - invulnStart)/CLOCKS_PER_SEC) >= 3.0) {
			/* Invulnerability must stop now. 3 Seconds passed. */
			invulnStart = thisClock;
			invuln = false;
		}
	}

	/* Kill the player if they fall for a certain period of time 
		(out of bounds normally...) */
	if (!colliding && velocityY <= -5.0) {
		if (((thisClock - fallStart)/CLOCKS_PER_SEC) >= 2.0) {
			kill();
			fallStart = thisClock;
		}
	} else {
		fallStart = thisClock;
	}

	/* Stop the entity from gotta go fast'ing. */
	if (velocityX >= 1.0) {
		velocityX = 1.0;
	} else if (velocityX <= -1.0) {
		velocityX = -1.0;
	}
	
	/* Based on the animation, choose a different row of the spritesheet. */
	switch(moveDir) {
	case MV_RIGHT:
		/* Fall through. */
	case MV_LEFT:
		frames = 3;
		t = 0.5f;
		v = 0.75f;
		break;
	case MV_UP:
		frames = 1;
		t = 0.0f;
		v = 0.25f;
		if (!faceLeft) {
			s = 0.0f;
			u = aniOffset;
		} else {
			u = 0.0f;
			s = aniOffset;
		}
		break;
	default:
		frames = 1;
		t = 0.75f;
		v = 1.0f;
		if (!faceLeft) {
			s = 0.0f;
			u = aniOffset;
		} else {
			u = 0.0f;
			s = aniOffset;
		}
		break;
	}

	if ((((double)thisClock - (double)curFrame)/(double)CLOCKS_PER_SEC) >= (double)(frameRate/frames)) {
		curFrame = thisClock;
		if (moveDir == MV_LEFT || moveDir == MV_RIGHT) {
			/* Handle frame changes properly. */
			if (aniRev) {
				frame--;
			} else {
				frame++;
			}
			/* If there is more than one frame, work out if we hit the last. */
			if (frame == 1) {
				/* If not, play the animation forward. */
				aniRev = false;
			} else if (frame == frames) {
				/* If so reverse the animation. */
				aniRev = true;
			}
			/* If the player isn't facing left, just set s/u normally. */
			if (!faceLeft) {
				s = aniOffset * (frame-1);
				u = s + aniOffset;
			} else {
				/* Otherwise swap them to flip the texture. */
				u = aniOffset * (frame-1);
				s = u + aniOffset;
			}
		}
	}
}

void Player::respawn() {

	xPos = spawnPos.x;
	yPos = spawnPos.y;
	velocityX = 0.0;
	velocityY = 0.0;
	invuln = true;
}

void Player::draw() {

	glPushMatrix();
		glEnable(GL_TEXTURE_2D); // Enable textures.
		if (invuln) {
			glColor4d(1.0, 1.0, 1.0, 0.75);
		} else {
			glColor4d(1.0, 1.0, 1.0, 1.0);
		}
		
		glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2f(s, t);
			glVertex3d(verts.vert[0].x, verts.vert[0].y, layer);
			glTexCoord2f(s, v);
			glVertex3d(verts.vert[3].x, verts.vert[3].y, layer);
			glTexCoord2f(u, t);
			glVertex3d(verts.vert[1].x, verts.vert[1].y, layer);
			glTexCoord2f(u, v);
			glVertex3d(verts.vert[2].x, verts.vert[2].y, layer);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	#ifdef SHOW_COLLISIONS
	glPushMatrix();
		glLineWidth(2.0);
		glLineStipple(1, 1);
		glColor3d(0.0, 0.0, 1.0);
		glBegin(GL_LINE_LOOP);
			glVertex3d(verts.cVert[3].x, verts.cVert[3].y, 0.9);
			glVertex3d(verts.cVert[0].x, verts.cVert[0].y, 0.9);
			glVertex3d(verts.cVert[1].x, verts.cVert[1].y, 0.9);
			glVertex3d(verts.cVert[2].x, verts.cVert[2].y, 0.9);
		glEnd();
		if (colliding) {
			glColor3d(0.0, 1.0, 0.0);
		} else {
			glColor3d(1.0, 0.0, 0.0);
		}
		glPointSize(4.0f);
		glBegin(GL_POINTS);
			glVertex3d(verts.cVert[3].x, verts.cVert[3].y, 1.0);
			glVertex3d(verts.cVert[0].x, verts.cVert[0].y, 1.0);
			glVertex3d(verts.cVert[1].x, verts.cVert[1].y, 1.0);
			glVertex3d(verts.cVert[2].x, verts.cVert[2].y, 1.0);
			glColor3d(0.0, 1.0, 1.0);
			glVertex3d(verts.center.x, verts.center.y, 1.0);
		glEnd();
	glPopMatrix();
	#endif
}

void Player::handleInput(double deltaT) {

	if (canJump() && ev.getKey(VK_SPACE))
		velocityY = 3.0;
	
	if (ev.getKey(0x41) || ev.getKey(VK_LEFT)) {
		velocityX -= xSpeed * deltaT;
		faceLeft = true;
	} 
	if (ev.getKey(0x44) || ev.getKey(VK_RIGHT)) {
		velocityX += xSpeed * deltaT;
		faceLeft = false;
	}
}

bool Player::canJump() {

	/* Check the player is standing on something and isn't falling/ climbing. */
	if (colliding)
		return (velocityY <= 0.1);

	return false;
}

void Player::modifyHealth(char h) {

	if (health + h <= maxHealth && health + h > 0) 
		health += h;
	else
		kill();
}

void Player::setTexture(GLuint t, int w, int h) {

	texID = t;
}

void Player::updateVerts() {

	/* Player needs their own version of this to handle the texture overlap. */
	verts.cVert[0].x = ((verts.vert[0].x = xPos) + width*0.25);
	verts.cVert[0].y = ((verts.vert[0].y = yPos) + height*0.025);
	verts.cVert[1].x = ((verts.vert[1].x = xWidth) - width*0.25);
	verts.cVert[1].y = ((verts.vert[1].y = yPos) + height*0.025);
	verts.cVert[2].x = ((verts.vert[2].x = xWidth) - width*0.25);
	verts.cVert[2].y = ((verts.vert[2].y = yHeight) - 0.1);
	verts.cVert[3].x = ((verts.vert[3].x = xPos) + width*0.25);
	verts.cVert[3].y = ((verts.vert[3].y = yHeight) - 0.1);

	verts.center.x = (verts.cVert[0].x + verts.cVert[2].x)/2;
	verts.center.y = (verts.cVert[0].y + verts.cVert[2].y)/2;
}

std::ostream& operator<<(std::ostream& out, const Player& p) {

	out << "ply(" << p.getXPos() << ", " << p.getYPos() << ", ";
	out << (int)p.getHealth() << ", " << (int)p.getLives() << ")";
	return out;
}

std::wistream& operator>>(std::wistream& in, Player& p) {

	double x, y;
	int hp, lv;
	wchar_t c1, c2, c3;

	if (in >> x >> c1 >> y >> c2 >> hp >> c3 >> lv) {
		if (c1 == ',' && c2 == ',' && c3 == ',') {
			p.setXPos(x);
			p.setYPos(y);
			p.setHealth(hp);
			p.setMaxHealth(hp);
			p.setLives(lv);
			p.setSpawnPoint(x, y);
			p.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "Player input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

void Player::respond(Platform& ent, Vertex& mtd) {

	/* If the Player goes too far into a Platform, respawn them. */
	if (mtd > 0.25) {
		modifyHealth(-1);
		respawn();
		return;
	}

	xPos = (xPos + mtd.x);
	yPos = (yPos + mtd.y);
	/* If the player is colliding with the top of the platform. */
	if (mtd.y > 0.0) {
		/* Push them out and set them to colliding. */
		velocityY = 0.0;
		colliding = true;
	} else if (mtd.y < 0.0) {
		/* Otherwise push them out and don't collide them. */
		velocityY = -0.1;
		colliding = false;
	}
}

void Player::respond(MovingPlatform& ent, Vertex& mtd) {

	/* If the Player goes too far into a Platform, respawn them. */
	if (mtd > 0.25) {
		modifyHealth(-1);
		respawn();
		return;
	}

	xPos = (xPos + mtd.x);
	//yPos = (yPos + mtd.y);
	if (fabs(ent.getXVelocity()) > fabs(velocityX)) {
			velocityX = ent.getXVelocity();
	}
	/* If the player is colliding with the top of the platform. */
	if (mtd.y > 0.0) {
		/* Push them out and set them to colliding. */
		colliding = true;
		if (velocityY < ent.getYVelocity())
			velocityY = ent.getYVelocity();
	} else if (mtd.y < 0.0) {
		/* Otherwise push them out and don't collide them. */
		velocityY = -0.1;
		colliding = false;
	}
}

void Player::respond(JumpingEnemy& ent, Vertex& mtd) {

	/* Throw the player clear. */
	velocityX = -velocityX;
	velocityY = 2;
	if (!isInvuln()) {
		modifyHealth(-ent.getDamage());
		startInvuln(); // Stop damage for 3 seconds.
	}
}

void Player::respond(WalkingEnemy& ent, Vertex& mtd) {

	velocityX = -velocityX;
	velocityY = 2;
	if (!isInvuln()) {
		modifyHealth(-ent.getDamage()); // Hurt the player.
		startInvuln(); // Stop damage for 3 seconds.
	}
}

void Player::respond(GameGoal& ent, Vertex& mtd) {

	std::cout << "THET" << std::endl;
	/* If there is a point goal, only let the player win when they have enough points. */
	if (ent.getPointRequirement() != 0 && totalPoints < ent.getPointRequirement()) {
		return;
	} else {
		/* Otherwise, see if there is a time goal. */
		if (ent.getTimeRequirement() != 0) {
			/* Get a point for every second spare. */
			clock_t diff = ent.getTimeRequirement() - ent.getCurrentTime();
			if (diff > 0) {
				/* Calculate bonus points. */
				int points = diff/CLOCKS_PER_SEC;
				modifyPoints(points);
			}
		}
		/* Finish the game. */
		ev.setGameState(HI_SCORES);
	}
}