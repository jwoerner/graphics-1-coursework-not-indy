#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>
#include <math.h>

#include "Polyshape.h"

void printMat(double *);
void setIdentity(double *);
void setRotMat(double *, double, int);
void setTraMat(double *, double, double, double);
void MultMat(double *,double *,double *);
void MultMatPostVec(double *M, double *vo, double *v);
void MultMatPreVec(double *M, double *vo, double *v);
void MultMatPre2DPoint(double *M, Vertex *v, Vertex *vN);

#endif