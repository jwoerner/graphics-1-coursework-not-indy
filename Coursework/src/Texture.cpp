#include "include\Texture.h"
#include "include\glext.h"

#ifndef GL_ARB_texture_compression
#define GL_ARB_texture_compression 1

#define GL_COMPRESSED_ALPHA_ARB					0x84E9
#define GL_COMPRESSED_LUMINANCE_ARB				0x84EA
#define GL_COMPRESSED_LUMINANCE_ALPHA_ARB		0x84EB
#define GL_COMPRESSED_INTENSITY_ARB				0x84EC
#define GL_COMPRESSED_RGB_ARB					0x84ED
#define GL_COMPRESSED_RGBA_ARB					0x84EE
#define GL_TEXTURE_COMPRESSION_HINT_ARB			0x84EF
#define GL_TEXTURE_COMPRESSED_IMAGE_SIZE_ARB	0x86A0
#define GL_TEXTURE_COMPRESSED_ARB				0x86A1
#define GL_NUM_COMPRESSED_TEXTURE_FORMATS_ARB	0x86A2
#define GL_COMPRESSED_TEXTURE_FORMATS_ARB		0x86A3

#endif

Texture::Texture() {

	width = 0;
	height = 0;
	texID = 0;
	mode = TGA_DEFAULT;
	fileName;
	format = GL_NONE;
	type = GL_NONE;
	imgData = NULL;
}

Texture::Texture(const int m) {

	width = 0;
	height = 0;
	texID = 0;
	mode = m;
	fileName;
	format = GL_NONE;
	type = GL_NONE;
	imgData = NULL;
}


bool Texture::loadTexture(std::wstring tex) {

	/* This is the default search path for textures.
		Sub-directories can be added in the filename. */
	std::wstring texPath = L"resource\\textures\\";
	std::wstring ext = L".tga";

	fileName = tex; // Get the filename for this texture.
	std::wstring filePath = texPath + fileName + ext;
	_wfopen_s(&tgaFile, (filePath.c_str()), L"rb");

	if (tgaFile == NULL) {
		/* If the TGA file itself failed to open. */
		loadError("Could not load TGA file!");
		return false;
	}
	if(fread(&header, sizeof(header), 1, tgaFile) == 0) {
		/* If the header couldn't be read, throw the error. */
		loadError("Could not read header!");
		return false;
	}

	if (fread(data.header, sizeof(data.header), 1, tgaFile) == 0) {
		/* If the data header couldn't be read, show another error. */
		loadError("Could not read data header!");
		return false;
	}
	/* Get the width, height and bits per pixel from the header. */
	width = (data.header[1] * 256) + data.header[0];
	height = (data.header[3] * 256) + data.header[2];
	data.bpp = data.header[4];

	if ((width <= 0) || (height <= 0) || ((data.bpp != 24) && (data.bpp != 32)))
	{
		/* If we have a zero width/ height, or invalid bpp, show this error. */
		loadError("Invalid TGA dimensions/ bpp");
		return false;
	}

	/* Set the right colour type depending on the bpp. */
	if (data.bpp == 24) {
		type = data.type = GL_RGB;
	} else if (data.bpp == 32 && mode&TGA_ALPHA) {
		type = data.type = GL_RGBA;
	}

	data.bytespp = (data.bpp / 8); // 8 Bits in a byte!
	data.imgSize = (data.bytespp * (width * height));

	imgData = (GLubyte*)malloc(data.imgSize); // Assign some memory.
	if (imgData == NULL) {
		/* If the memory fails to allocate for some reason. */
		loadError("Could not allocate memory!");
		return false;
	}

	if (header.header[2] == 2) {
		/* Load an uncompressed TGA file. */
		if (!loadUnCompressed()) {
			return false;
		}
	} else if (header.header[2] == 10) {
		/* Load an compressed TGA file. */
		if (!loadCompressed()) {
			return false;
		}
	} else {
		/* Not sure what we're dealing with here... */
		loadError("Unknown TGA format!");
		return false;
	}

	fclose(tgaFile); // We're done with the TGA file now.

	/* Do OpenGL stuff to make this a valid texture. */
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	if (mode&TGA_MIPMAPS) {
		/* If mipmaps are requested, generate them using this. */
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	}
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, data.type, 
		GL_UNSIGNED_BYTE, imgData);

	/* Clear this memory now we're done. */
	if (imgData != NULL) {
		free(imgData);
	}

	return true;
}

bool Texture::loadUnCompressed() {

	if (fread(imgData, 1, data.imgSize, tgaFile) != data.imgSize) {
		/* Just copy the raw TGA data if possible. */
		loadError("Could not read TGA data!");
		return false;
	}

	for (GLuint i = 0; i < (int)data.imgSize; i += data.bytespp) {
		/* Flip the B and G values to make these RGB rather than RBG. */
		imgData[i] ^= imgData[i + 2] ^=
			imgData[i] ^= imgData[i + 2];
	}

	/* Select the real OpenGL format. */
	switch (data.type) {
	case GL_RGB:
		format = GL_RGB;
		break;
	case GL_RGBA:
		format = GL_RGBA;
		break;
	}

	return true;
}

bool Texture::loadCompressed() {

	int pixelCount = width * height; // We need the number of pixels...
	int curPixel = 0;
	int curByte = 0;

	GLubyte* colBuff = (GLubyte*)malloc(data.bytespp); // Allocate more mem!

	do {
		GLubyte chunk = 0;
		if (fread(&chunk, sizeof(GLubyte), 1, tgaFile) == 0) {
			/* If a chunk fails to read, there's a problem. */
			loadError("Failed to load chunk");
			if (colBuff != NULL) {
				/* Make sure the colour buffer is removed safely... */
				free(colBuff);
			}
			return false;
		}

		if (chunk < 128) {
			chunk++;

			for (short i = 0; i < chunk; i++) {
				if (fread(colBuff, 1, data.bytespp, tgaFile) != data.bytespp) {
					/* If a pixel value fails to read, give up. */
					loadError("Failed to read raw chunk colours!");
					if (colBuff != NULL) {
						free(colBuff);
					}
					return false;
				}
				/* Set the image data. */
				imgData[curByte] = colBuff[2];
				imgData[curByte + 1] = colBuff[1];
				imgData[curByte + 2] = colBuff[0];
				if (data.bytespp == 4) {
					imgData[curByte + 3] = colBuff[3];
				}
				/* Move on to the next pixel/ byte. */
				curByte += data.bytespp;
				curPixel++;

				if (curPixel > pixelCount) {
					/* If we somehow over-read the file, this should happen. */
					loadError("Read too many pixels...");
					if (colBuff != NULL) {
						free(colBuff);
					}
					return false;
				}
			}
		} else {
			/* More of the same from a small chunk. */
			chunk -= 127;

			if (fread(colBuff, 1, data.bytespp, tgaFile) != data.bytespp) {
				loadError("Failed to read chunk colours!");
				if (colBuff != NULL) {
					free(colBuff);
				}
				return false;
			}

			for (short i = 0; i < chunk; i++) {
				imgData[curByte] = colBuff[2];
				imgData[curByte + 1] = colBuff[1];
				imgData[curByte + 2] = colBuff[0];
				if (data.bytespp == 4) {
					imgData[curByte + 3] = colBuff[3];
				}

				curByte += data.bytespp;
				curPixel++;

				if (curPixel > pixelCount) {
					loadError("Read too many pixels...");
					if (colBuff != NULL) {
						free(colBuff);
					}
					return false;
				}
			}
		}		
	} while (curPixel < pixelCount);

	/* Select the proper compressed type for this texture. */
	switch (data.type) {
	case GL_RGB:
		format = GL_COMPRESSED_RGB_ARB;
		break;
	case GL_RGBA:
		format = GL_COMPRESSED_RGBA_ARB;
		break;
	}

	if (colBuff != NULL) {
		free(colBuff);
	}

	return true;
}

void Texture::loadError(char* errMsg) {

	std::cerr << errMsg << std::endl;
	if (tgaFile != NULL) {
		fclose(tgaFile);
	}
	if (this->imgData != NULL) {
		free(imgData);
	}
}