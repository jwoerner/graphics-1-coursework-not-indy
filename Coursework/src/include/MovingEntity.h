/*******************************************************************************
 *	Name:			MovingEntity
 *	Author:			Jordan Woerner
 *	Description:	MovingEntity describes an entity that can update its' 
					position and rotation. This class handles the gravity, 
					friction, speed and velocity of Objects, extending 
					StaticEntity for position attributes.
 ******************************************************************************/
#ifndef MOVING_ENTITY_H
#define MOVING_ENTITY_H

#include "StaticEntity.h"

/*
 *	Enumeration of Directions. Useful for MovingEntities.
 */
enum Directions {

	MV_LEFT,
	MV_UP,
	MV_RIGHT,
	MV_DOWN
};

class MovingEntity : public StaticEntity
{
protected:
	bool applyGravity;	// Apply gravity to this entity.
	bool applyFriction;	// Apply friction to this entity.
	double xSpeed;		// The acceleration speed along X.
	double ySpeed;		// The acceleration speed along Y.
	double velocityX;	// The speed the player is moving along X.
	double velocityY;	// The speed the player is moving along Y.

public:
	MovingEntity();

	MovingEntity(double x, double y, double w, double h, double r, double xS, double yS);

	/*
	 setGravity(bool)
	 Enables/ disables gravity on this entity.
	 */
	inline void setGravity(bool b) { applyGravity = b; }
	/*
	 setFriction(bool)
	 Enables/ disables friction on this entity.
	 */
	inline void setFriction(bool f) { applyFriction = f; }
	/*
	 setXSpeed(double)
	 Sets the speed this entity accelerates along the X axis at.
	 */
	inline void setXSpeed(double x) { xSpeed = x; }
	/*
	 setYSpeed(double)
	 Sets the speed this entity accelerates along the Y axis at.
	 */
	inline void setYSpeed(double y) { ySpeed = y; }
	/*
	 setXVelocity(double)
	 Sets the X velocity of this entity.
	 */
	inline void setXVelocity(double x) { velocityX = x; }
	/*
	 setYVelocity(double)
	 Sets the Y velocity of this entity.
	 */
	inline void setYVelocity(double y) { velocityY = y; }

	/*
	 const bool gravityEnabled()
	 Returns true if the entity has gravity enabled. False otherwise.
	 */
	inline bool gravityEnabled() const { return applyGravity; }
	/*
	 const bool frictionEnabled()
	 Returns true if the entity has friction applied. False otherwise.
	 */
	inline bool frictionEnabled() const { return applyFriction; }
	/*
	 const double getXSpeed()
	 Returns the speed this entity accelerates along X at.
	 */
	inline double getXSpeed() const { return xSpeed; }
	/*
	 const double getYSpeed()
	 Returns the speed this entity accelerates along Y at.
	 */
	inline double getYSpeed() const { return ySpeed; }
	/*
	 const double getXVelocity()
	 Returns the X velocity of this entity.
	 */
	inline double getXVelocity() const { return velocityX; }
	/*
	 const double getYVelocity()
	 Returns the Y velocity of this entity.
	 */
	inline double getYVelocity() const { return velocityY; }

	virtual void update(double deltaT);
};

#endif