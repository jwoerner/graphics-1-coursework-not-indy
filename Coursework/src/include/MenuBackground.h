/*******************************************************************************
 *	Name:			MenuBackground
 *	Author:			Jordan Woerner
 *	Description:	A background for menus throughout the game.
 ******************************************************************************/
#ifndef MANU_BACKGROUND_H
#define MENU_BACKGROUND_H

#include "BaseIncludes.h"

class MenuBackground
{
private:
	GLuint texID;	// OpenGL reference to the texture to use.

public:
	/*
	 MenUBackground()
	 Creates a new MenuBackground.
	 */
	MenuBackground();

	/*
	 setTexture(LGuint)
	 Sets the texture to use to the provided reference ID.
	 */
	inline void setTexture(GLuint tex) { texID = tex; }
	/*
	 GLuint getTexture() const
	 Gets the reference ID for the texture this background is using.
	 */
	inline GLuint getTexture() const { return texID; }

	void draw();
};

#endif