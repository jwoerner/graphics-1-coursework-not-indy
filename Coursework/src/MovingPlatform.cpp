#include "include\MovingPlatform.h"
#include "include\BaseIncludes.h"
#include <math.h>

MovingPlatform::MovingPlatform() {

	layer = SCENERY_B;
	startXPos = 0.0;
	startYPos = 0.0;
	distance = 0.0;
	timer = 0.0;
	dir = Directions(0);
	reversed = false;
	solid = true;
}

MovingPlatform::MovingPlatform(double x, double y, double w, double h, double r, 
	Directions d, double dist, double spd) : MovingEntity(x, y, w, h, r, spd, spd) {

		layer = SCENERY_B;
		startXPos = x;
		startYPos = y;
		timer = 0.0;
		distance = dist;
		dir = d;
		reversed = false;
		solid = true;
}

void MovingPlatform::update(double deltaT) {

	MovingEntity::update(deltaT);

	timer += deltaT;

	if (timer <= 2.0)
		return;

	/* Handle the platform movement, accelerate hardcore. */
	switch (dir) {
	case MV_RIGHT:
		if (!reversed) {
			if (velocityX <= xSpeed) {
				velocityX += (xSpeed * deltaT);
			}
		} else {
			if (velocityX >= -xSpeed) {
				velocityX -= (xSpeed * deltaT);
			}
		}
		break;
	case MV_LEFT:
		if (!reversed) {
			if (velocityX >= -xSpeed) {
				velocityX -= (xSpeed * deltaT);
			}
		} else {
			if (velocityX <= xSpeed) {
				velocityX += (xSpeed * deltaT);
			}
		}
		break;
	case MV_UP:
		if (!reversed) {
			if (velocityY <= ySpeed) {
				velocityY += (ySpeed * deltaT);
			}
		} else {
			if (velocityY >= -ySpeed) {
				velocityY -= (ySpeed * deltaT);
			}
		}
		break;
	case MV_DOWN:
		if (!reversed) {
			if (velocityY >= -ySpeed) {
				velocityY -= (ySpeed * deltaT);
			}
		} else {
			if (velocityY <= ySpeed) {
				velocityY += (ySpeed * deltaT);
			}
		}
		break;
	}

	/* Reverse the direction of the platform if it reaches max distance. */
	if (fabs(yPos - startYPos) >= distance
		|| fabs(xPos - startXPos) >= distance) {
			reverse();
	}
}

void MovingPlatform::reverse() {

	startXPos = xPos;
	startYPos = yPos;
	velocityX = 0.0;
	velocityY = 0.0;
	timer = 0.0;
	reversed = !reversed;
}

std::wistream& operator>>(std::wistream& in, MovingPlatform& p) {

	double w, h, x, y, dist, spd;
	int dir;
	wchar_t c1, c2, c3, c4, c5, c6;

	if (in >> x >> c1 >> y >> c2 >> w >> c3 >> h >>
		c4 >> dir >> c5 >> dist >> c6 >> spd) {
			if (c1 == ',' && c2 == ',' && c3 == ','
				&& c4 == ',' && c5 == ',' && c6 == ',') {
					if (dir <= 3) {
						p.setXPos(x);
						p.setYPos(y);
						p.setWidth(w);
						p.setHeight(h);
						p.setXSpeed(spd);
						p.setYSpeed(spd);
						p.setDistance(dist);
						p.setDirection(Directions(dir));
					} else {
						in.setstate(std::ios_base::failbit);
						std::cerr << "MovingPlatform direction is not valid!" 
							<< std::endl;
					}
			} else {
				in.setstate(std::ios_base::failbit);
				std::cerr << "MovingPlatform input not properly formed!" 
					<< std::endl;
			}
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const MovingPlatform& p) {

	out << "mvplat(" << p.getXPos() << ", " << p.getYPos();
	out << ", " << p.getWidth() << ", " << p.getHeight();
	out << ", " << p.getDirection() << ", " << p.getXSpeed();
	out << ", " << p.getDistance() << ")";
	return out;
}

void MovingPlatform::respond(MovingPlatform& ent, Vertex& mtd) {

	/* Push this platform out, and reverse it. */
	xPos += mtd.x*2;
	yPos += mtd.y*2;
	reverse();
	/* Push the colliding platform out and reverse that. */
	ent.setXPos(ent.getXPos() - mtd.x*2);
	ent.setYPos(ent.getYPos() - mtd.y*2);
	ent.reverse();
}