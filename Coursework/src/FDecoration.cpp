#include "include\FDecoration.h"

FDecoration::FDecoration() {

	layer = SCENERY_F;
}

FDecoration::FDecoration(double x, double y, double w, double h, double r) 
	: Decoration(x, y, w, h, r) {

		layer = SCENERY_F;
}

std::wistream& operator >>(std::wistream& in, FDecoration& d) {

	double w, h, x, y;
	wchar_t c1, c2, c3;

	if (in >> x >> c1 >> y >> c2 >> w >> c3 >> h) {
		if (c1 == ',' && c2 == ',' && c3 == ',') {
			d.setXPos(x);
			d.setYPos(y);
			d.setWidth(w);
			d.setHeight(h);
			d.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "FDecoration input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

std::ostream& operator <<(std::ostream& out, const FDecoration& d) {

	out << "fdecor(" << d.getXPos() << ", " << d.getYPos() << ", ";
	out << d.getWidth() << ", " << d.getHeight() << ")";

	return out;
}