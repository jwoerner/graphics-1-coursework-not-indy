#include "include\WalkingEnemy.h"

WalkingEnemy::WalkingEnemy() {

	xSpeed = 0.0;
	walkDist = 0.0;
	walkLeft = false;
}

WalkingEnemy::WalkingEnemy(double spd, double dist, bool left) {

	xSpeed = spd;
	walkDist = dist;
	walkLeft = left;
}

WalkingEnemy::WalkingEnemy(double x, double y, double w, double h, unsigned char hp, char dam, double spd, double dist, bool left) 
	: BaseEnemy(x, y, w, h, hp, dam) {

		xSpeed = spd;
		walkDist = dist;
		walkLeft = left;
}

void WalkingEnemy::update(double deltaT) {

	MovingEntity::update(deltaT);

	if (walkLeft) {
		/* Flip the enemy texture if they are going left. */
		s = 0.0f;
		u = 1.0f;
		if (velocityX > -xSpeed)
			velocityX -= (xSpeed * deltaT);
		else
			velocityX = -xSpeed;
	} else {
		/* Flip them to face right. */
		s = 1.0f;
		u = 0.0f;
		if (velocityX < xSpeed)
			velocityX += (xSpeed * deltaT);
		else
			velocityX = xSpeed;
	}

	if (fabs(xPos - startXPos) > walkDist) {
		walkLeft = !walkLeft;
		startXPos = xPos;
	}
}

std::wistream& operator>>(std::wistream& iStr, WalkingEnemy& en) {

	double w, h, x, y, spd, dist;
	unsigned int hp, d;
	wchar_t c1, c2, c3, c4, c5, c6, c7, c8, left;

	if (iStr >> x >> c1 >> y >> c2 >> w >> c3 >> h >> c4 >> hp >> c5 >> d >> c6 >> spd >> c7 >> dist >> c8 >> left) {
		if (c1 == ',' && c2 == ',' && c3 == ',' && c4 == ',' 
			&& c5 == ',' && c6 == ',' && c7 == ',' && c8 == ',') {
				en.setXPos(x);
				en.setYPos(y);
				en.setWidth(w);
				en.setHeight(h);
				en.setHealth(hp);
				en.setDamage(d);
				en.setXSpeed(spd);
				en.setWalkDistance(dist);
				/* If the last thing is 'l', the enemy moves left. */
				en.setWalkLeft(left == 'l' ? false : true);
				en.updateVerts();
		} else {
			iStr.setstate(std::ios_base::failbit);
			std::cerr << "WalkingEnemy input not properly formed!" 
				<< std::endl;
		}
	}

	return iStr;
}

std::ostream& operator<<(std::ostream& oStr, const WalkingEnemy& w) {

	oStr << "walken(" << w.getXPos() << ", " << w.getYPos() << ", " << w.getWidth() << ", " << w.getHeight();
	oStr << ", " << (int)w.getHealth() << ", " << (int)w.getDamage() << ", ";
	oStr << w.getXSpeed() << ", " << w.getWalkDistance() << ", " << w.isWalkingleft() << ")";

	return oStr;
}

void WalkingEnemy::respond(Platform& ent, Vertex& mtd) {

	xPos += mtd.x;
	yPos += mtd.y;

	if (mtd.x != 0.0)
		walkDist = fabs(xPos - startXPos);
	
	if (mtd.y > 0.0)
		velocityY = 0.0;
}