#include "include\GameGoal.h"
#include "include\Player.h"

GameGoal::GameGoal(EventHandler& handler) : ev(handler) {

	pointGoal = 0;
	timeGoal = 0;
	curTime = 0;
	solid = true;
}

GameGoal::GameGoal(EventHandler& handler ,double x, double y) 
	: ev(handler), StaticEntity(x, y, 1.0, 1.0, 0.0) {

	pointGoal = 0;
	timeGoal = 0;
	curTime = 0;
	solid = true;
}

void GameGoal::update(double deltaT) {

	if(curTime < timeGoal) {
		curTime = clock();
	}
}

std::wistream& operator>>(std::wistream& iStr, GameGoal& g) {
	
	double x, y, w, h;
	clock_t p, t;
	wchar_t c1, c2, c3, c4, c5;

	if(iStr >> x >> c1 >> y >> c2 >> w >> c3 >> h >> c4 >> t >> c5 >> p) {
		if (c1 == ',' && c2 == ',' && c3 == ',' && c4 == ',' && c5 == ',') {
			t *= CLOCKS_PER_SEC;
			g.setXPos(x);
			g.setYPos(y);
			g.setWidth(w);
			g.setHeight(h);
			g.setPointRequirement(p);
			g.setTimeRequirement(t);
			g.updateVerts();
		} else {
			iStr.setstate(std::ios::failbit);
			std::cerr << "Gamegoal not correctly formed!" << std::endl;
		}
	}
	return iStr;
}

std::ostream& operator<<(std::ostream& oStr, const GameGoal& g) {
	
	oStr << "goal(" << g.getXPos() << ", " << g.getYPos() << ", ";
	oStr << g.getWidth() << ", " << g.getHeight() << ", ";
	oStr << g.getTimeRequirement() << ", " << g.getPointRequirement() << ")";

	return oStr;
}

void GameGoal::respond(Player& ply, Vertex& mtd) {

	/* If there is a point goal, only let the player win when they have enough points. */
	if (pointGoal != 0 && ply.getPoints() < pointGoal) {
		return;
	} else {
		/* Otherwise, see if there is a time goal. */
		if (timeGoal != 0) {
			/* Get a point for every second spare. */
			clock_t diff = timeGoal - curTime;
			if (diff > 0) {
				/* Calculate bonus points. */
				int points = diff/CLOCKS_PER_SEC;
				ply.modifyPoints(points);
			}
		}
		/* Finish the game. */
		ev.setGameState(HI_SCORES);
	}
}
