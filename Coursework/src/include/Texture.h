/*******************************************************************************
 *	Name:			Texture
 *	Author:			Jordan Woerner
 *	Description:	Texture loader for TGA textures. Supports mipmaps and alpha 
					channels.
 ******************************************************************************/
#ifndef TEXTURE_H
#define TEXTURE_H

#include "BaseIncludes.h"
#include <string>

#define TGA_DEFAULT	0x0000000000000000
#define TGA_MIPMAPS	0x0000000000000001
#define TGA_ALPHA	0x0000000000000010

class Texture
{
private:
	/*
	 Stores some information about the TGA file contents.
	 */
	typedef struct {
		GLubyte header[12];
	} TGAHeader;
	/*
	 Stores the actual information from the TGA file.
	 */
	typedef struct {
		GLubyte header[6];
		GLuint bytespp;
		GLuint bpp;
		GLuint imgSize;
		GLuint type;
	} TGAData;

	TGAHeader header;	// An instance of the header.
	TGAData data;		// An instance of the TGA data.
	FILE* tgaFile;		// The TGA file we are using.
	std::wstring fileName;	// The filename of the TGA file.
	int width;			// The width of the texture in pixels.
	int height;			// The height of the texture in pixels.
	unsigned int mode;	// The TGA flags for things like Alpha and Mipmaps.
	GLuint type;		// The colour type of the texture.
	GLubyte* imgData;	// The image data.
	GLenum format;		// The colour format for the image.
	GLuint texID;		// The OpenGL reference to this Texture.

	/*
	 bool loadUnCompressed()
	 Loads an uncompressed TGA file.
	 */
	bool loadUnCompressed();
	/*
	 bool loadCompressed()
	 Loads a compressed TGA file (RLE compression).
	 */
	bool loadCompressed();
	/*
	 loadError(char*)
	 Displays the specified error message and cleans up memory.
	 */
	void loadError(char* errMsg);

public:
	/*
	 Texture()
	 Creates a TGA texture with no special features enabled.
	 */
	Texture();
	/*
	 Texture(int)
	 Creates a new texture with the specified TGA flags set.
	 */
	Texture(const int m);

	/*
	 bool loadTexture(std::string)
	 Loads the specified texture from the texture directory.
	 Returns true if the texture is successfully made. False otherwise.
	 */
	bool loadTexture(std::wstring tex);

	/*
	 std::string getTextureName()
	 Returns the file name for this texture.
	 */
	inline std::wstring getTextureName() const {

		return fileName;
	}
	/*
	 GLuint getTextureID()
	 Returns the OpenGL reference ID for this texture.
	 */
	inline GLuint getTextureID() const {

		return texID;
	}
	/*
	 int getWith()
	 Gets the width of this texture in pixels.
	 */
	inline int getWidth() const {

		return width;
	}
	/*
	 int getHeight()
	 Returns the height of this texture in pixels.
	 */
	inline int getHeight() const {

		return height;
	}
	
};

#endif