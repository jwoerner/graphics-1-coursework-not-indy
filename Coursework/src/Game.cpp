#include "include\Game.h"
#include "include\BaseIncludes.h"
#include "include\Camera.h"
#include "include\Collision.h"
#include "include\GameGoal.h"
#include "include\Decoration.h"
#include "include\FDecoration.h"
#include "include\Platform.h"
#include "include\DPlatform.h"
#include "include\MovingPlatform.h"
#include "include\PointPickup.h"
#include "include\Spike.h"
#include "include\Follower.h"
#include "include\JumpingEnemy.h"
#include "include\WalkingEnemy.h"
#include "include\FreeType.h"
#include "include\Checkpoint.h"
#include "include\Background.h"

#include <fstream>
#include <algorithm>
#include <tchar.h>
#include <string>

/* Macro definition to stop me having to type this all the time. */
#define ENTS_IT std::vector<std::shared_ptr<StaticEntity>>::iterator

Game::Game(EventHandler& handler) : ev(handler), cam(handler) {

	ents = std::vector<std::shared_ptr<StaticEntity>>();
	uiFont.init("resource/fonts/arial.ttf", 48);
	uiFontSmall.init("resource/fonts/arial.ttf", 24);
	colliding = 0;
	firstUpdate = true;
	boundTex = 0;
	seconds = 0;
}

void Game::initMainMenu() {

	glDeleteTextures(1, &hud);
	if (ents.size() > 0) {
		cleanUpGL();
		ents.clear();
	}
	Texture t(TGA_ALPHA | TGA_MIPMAPS);
	t.loadTexture(L"titlescreen");
	bg.setTexture(t.getTextureID());
	ev.setGameState(MAIN_MENU);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	cam.reset();
	cam.follow(NULL);
}

void Game::initLevelMenu() {

	GLuint tex = bg.getTexture();
	glDeleteTextures(1, &tex);
	if (ents.size() > 0) {
		cleanUpGL();
		ents.clear();
	}
	if (levels.size() > 0) {
		levels.clear();
	}
	populateLevelMenu();
	Texture t(TGA_ALPHA | TGA_MIPMAPS);
	t.loadTexture(L"levelselect");
	bg.setTexture(t.getTextureID());
}

void Game::populateLevelMenu() {

	HANDLE foundFile = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA found;

	foundFile = FindFirstFile(_T("resource\\levels\\*.lev"), &found);
	if (foundFile == INVALID_HANDLE_VALUE) {
		std::cerr << "Could not find files!" <<std::endl;
		exit(-2);
	} else {
		unsigned int n = 1;
		do {
			LevelBox lvl(found.cFileName, n);
			Texture t(TGA_MIPMAPS | TGA_ALPHA);
			if (t.loadTexture(L"levels\\" + lvl.getLevel().substr(0, lvl.getLevel().find('.'))))
				lvl.setTexture(t.getTextureID());
			levels.push_back(lvl);
			std::wcout << levels.back().getLevel() << std::endl;
			n++;
		} while (FindNextFile(foundFile, &found) != 0);
	}
}

void Game::startGame(std::wstring lvlName) {

	GLuint tex = bg.getTexture();
	glDeleteTextures(1, &tex);
	cleanUpGL();
	levels.clear();
	if (ents.size() > 0) {
		ents.clear();
	}
	
	if (loadLevel(lvlName)) {
		Texture t(TGA_ALPHA | TGA_MIPMAPS);
		t.loadTexture(L"hudicons");
		hud = t.getTextureID();
		/* If the level loads, set the game up properly. */
		cam.follow(ply.get());
		ev.setGameState(PLAYING);
	} else {
		/* If the level fails to load. Show this. */
		std::wstring str;
		str.append(L"Failed to load level ").append(lvlName).append(L"!");
		MessageBox(NULL, (LPCWSTR)str.c_str(), L"Level Missing", MB_OK | MB_ICONWARNING);
		initLevelMenu(); // Return to the level menu.
	}
}

void Game::initWinScreen() {

	cleanUpGL();
	if (ents.size() > 0) {
		ents.clear();
	}
	Texture t(TGA_ALPHA | TGA_MIPMAPS);
	t.loadTexture(L"winscreen");
	bg.setTexture(t.getTextureID());
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	cam.reset();
	cam.follow(NULL);
}

void Game::initLoseScreen() {

	cleanUpGL();
	if (ents.size() > 0) {
		ents.clear();
	}
	Texture t(TGA_ALPHA | TGA_MIPMAPS);
	t.loadTexture(L"losescreen");
	bg.setTexture(t.getTextureID());
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	cam.reset();
	cam.follow(NULL);
}

void Game::checkCollisions() {

	switch (oldState) {
	case PLAYING:
		/* If the game is playing we want to check collisions. */
		for (unsigned int i = 0; i < ents.size(); i++) {
			if (!ents.at(i)->isSolid())
				continue; // Skip this entity if it isn't solid.
			for(unsigned int j = i+1; j < ents.size(); j++) {
				if (!ents.at(j)->isSolid())
					continue; // Skip this check if it isn't solid.
				/* Check every entity against everything but itself. */
				colVec = calcMTD(&ents.at(i)->verts, &ents.at(j)->verts);
				if (colVec != 0.0) {
					/* A collision was detected. Now the entity collides. */
					ents.at(i)->collide(*ents.at(j).get(), colVec);
				}				
			}
		}
		break;
	}
}

void Game::update(double deltaT) {

	GameState newState = ev.getGameState();
	/* Every iteration, check the current state against whatever the 
		EventHandler wants to use. */
	if (newState != oldState) {
		/* If it has changed, run the right stuff and update the state. */
		switch(newState) {
		case MAIN_MENU:
			initMainMenu();
			break;
		case PLAYING:
			if (oldState == LEVEL_MENU)
				/* If we are starting the level, set the time requirement correctly. */
				if (goal->getTimeRequirement() > 0)
					goal->setTimeRequirement(goal->getTimeRequirement() + clock());
			if (oldState == PAUSE)
				/* If we are unpausing the game, add the time elapsed during pause. */
				if (goal->getTimeRequirement() > 0)
					goal->setTimeRequirement(goal->getTimeRequirement() + (clock() - pauseStart));
			break;
		case PAUSE:
			/* Record when pausing began. */
			pauseStart = clock();
			break;
		case LEVEL_MENU:
			initLevelMenu();
			break;
		case GAME_OVER:
			/* Remove anything we don't need now. */
			initLoseScreen();
			break;
		case HI_SCORES:
			/* Set up the win screen. */
			initWinScreen();
			break;
		}
		oldState = newState;
	}
	/* Update the game using the currently stored state. */
	switch (oldState) {
	case MAIN_MENU:
		/* Going to the main menu resets the game, so the first update must
			run again. */
		firstUpdate = true;
		if (ev.getKey(VK_RETURN)) {
			ev.setGameState(LEVEL_MENU);
			std::cout << "Opened level menu." << std::endl;
			break;
		}
		break;
	case LEVEL_MENU:
		if (levels.size() != 0) {
			if (ev.getKey(VK_NUMPAD1) || ev.getKey(0x31)) {
				startGame(levels.at(0).getLevel());
				break;
			}

			if (levels.size() >= 2) {
				if (ev.getKey(VK_NUMPAD2) || ev.getKey(0x32)) {
					startGame(levels.at(1).getLevel());
					break;
				}
			}

			if (levels.size() >= 3) {
				if (ev.getKey(VK_NUMPAD3) || ev.getKey(0x33)) {
					startGame(levels.at(2).getLevel());
					break;
				}
			}

			if (levels.size() >= 4) {
				if (ev.getKey(VK_NUMPAD4) || ev.getKey(0x34)) {
					startGame(levels.at(3).getLevel());
					break;
				}
			}

			if (levels.size() >= 5) {
				if (ev.getKey(VK_NUMPAD5) || ev.getKey(0x35)) {
					startGame(levels.at(4).getLevel());
					break;
				}
			}

			if (levels.size() >= 6) {
				if (ev.getKey(VK_NUMPAD6) || ev.getKey(0x36)) {
					startGame(levels.at(5).getLevel());
					break;
				}
			}
		}

		if (ev.getKey(VK_ESCAPE)) {
			ev.setGameState(MAIN_MENU);
			std::cout << "Returned to main menu." << std::endl;
			break;
		}
		break;
	case PLAYING:
		if (firstUpdate) {
			/* The first update is running now, false this. */
			firstUpdate = false;
		}
		if (ev.getKey(VK_HOME)) {
			ev.setGameState(PAUSE); // Stop the game from simulating.
			std::cout << "Game Paused" << std::endl;
			break;
		}
		/* If the game is currently playing, update the world. */
		for (ENTS_IT it = ents.begin(); it != ents.end(); ++it) {
			it->get()->update(deltaT);
			it->get()->setColliding(false);
		}
		cam.update(deltaT);
		seconds = (goal->getTimeRequirement() - goal->getCurrentTime())/CLOCKS_PER_SEC;
		break;
	case PAUSE:
		/* If the game is paused, we don't need to update the world. */
		/* Though we do need to keep track of user input anyway. */
		if (ev.getKey(VK_END)) {
			ev.setGameState(PLAYING); // Resume the game.
			std::cout << "Game Resumed" << std::endl;
			break;
		}
		if (ev.getKey(VK_ESCAPE)) {
			ev.setGameState(MAIN_MENU);
			std::cout << "Quit to menu." << std::endl;
			break;
		}
		break;
	case HI_SCORES:
		if (ev.getKey(VK_ESCAPE)) {
			ev.setGameState(MAIN_MENU);
			std::cout << "Returned to main menu." << std::endl;
		}
		break;
	case GAME_OVER:
		if (ev.getKey(VK_ESCAPE)) {
			/* Quit to main menu. */
			ev.setGameState(MAIN_MENU);
			std::cout << "Returned to main menu." << std::endl;
		}
		break;
	}
}

void Game::display() {

	switch (oldState) {
	case MAIN_MENU:	
		bg.draw();
		glColor3d(1.0, 1.0, 1.0);
		freetype::print(uiFont, 125, 100, "Press Enter to play!");
		freetype::print(uiFontSmall, 25, 25, "(Press F12 to exit)");
		break;
	case LEVEL_MENU:
		bg.draw();
		for (unsigned int i = 0; i < levels.size(); i++) {
			levels.at(i).draw();
		}
		freetype::print(uiFontSmall, 25, 25, "Press 'ESC' to go back");
		break;
	case PLAYING:
		/* If the game is currently playing, render the world. */
		for (ENTS_IT it = ents.begin(); it != ents.end(); ++it) {
			if (it->get()->getTexture() != boundTex) {
				glBindTexture(GL_TEXTURE_2D, it->get()->getTexture());
				boundTex = it->get()->getTexture();
			}
			it->get()->draw();
		}
		/* Render the heads up display. */
		glPushMatrix();
			glBindTexture(GL_TEXTURE_2D, hud);
			glEnable(GL_TEXTURE_2D);
			glTranslated(cam.getX(), cam.getY(), 0.0);
			glBegin(GL_QUADS);
			for (int i = 0; i < ply->getLives(); i++) {
				glTexCoord2d(0.5, 0.0); glVertex2d(-6.5 + (1.5*i), -5.0);
				glTexCoord2d(0.0, 0.0); glVertex2d(-8.0 + (1.5*i), -5.0);
				glTexCoord2d(0.0, 1.0); glVertex2d(-8.0 + (1.5*i), -3.5);
				glTexCoord2d(0.5, 1.0); glVertex2d(-6.5 + (1.5*i), -3.5);
			}
			for (int i = 0; i < ply->getHealth(); i++) {
				glTexCoord2d(1.0, 0.0); glVertex2d(-6.5 + (1.5*i), -3.0);
				glTexCoord2d(0.5, 0.0); glVertex2d(-8.0 + (1.5*i), -3.0);
				glTexCoord2d(0.5, 1.0); glVertex2d(-8.0 + (1.5*i), -1.5);
				glTexCoord2d(1.0, 1.0); glVertex2d(-6.5 + (1.5*i), -1.5);
			}
			glEnd();
			glDisable(GL_TEXTURE_2D);
		glPopMatrix();
		glPushMatrix();
			glLoadIdentity();
			freetype::print(uiFont, 275, 30, "Points: %d", ply->getPoints());
			if (goal->getTimeRequirement() > 0) {
				freetype::print(uiFontSmall, 310, 530, "Bonus Time:");
				freetype::print(uiFont, 325, 475, "%ds", seconds);
			}
		glPopMatrix();
		break;
	case PAUSE:
		/* If the game is paused, draw the world, and the pause menu. */
		for (ENTS_IT it = ents.begin(); it != ents.end(); ++it) {
			if (it->get()->getTexture() != boundTex) {
				glBindTexture(GL_TEXTURE_2D, it->get()->getTexture());
				boundTex = it->get()->getTexture();
			}
			it->get()->draw();
		}
		/* Continue rendering the HUD. */
		glPushMatrix();
			glBindTexture(GL_TEXTURE_2D, hud);
			glEnable(GL_TEXTURE_2D);
			glTranslated(cam.getX(), cam.getY(), 0.0);
			glBegin(GL_QUADS);
			for (int i = 0; i < ply->getLives(); i++) {
				glTexCoord2d(0.5, 0.0); glVertex2d(-6.5 + (1.5*i), -5.0);
				glTexCoord2d(0.0, 0.0); glVertex2d(-8.0 + (1.5*i), -5.0);
				glTexCoord2d(0.0, 1.0); glVertex2d(-8.0 + (1.5*i), -3.5);
				glTexCoord2d(0.5, 1.0); glVertex2d(-6.5 + (1.5*i), -3.5);
			}
			for (int i = 0; i < ply->getHealth(); i++) {
				glTexCoord2d(1.0, 0.0); glVertex2d(-6.5 + (1.5*i), -3.0);
				glTexCoord2d(0.5, 0.0); glVertex2d(-8.0 + (1.5*i), -3.0);
				glTexCoord2d(0.5, 1.0); glVertex2d(-8.0 + (1.5*i), -1.5);
				glTexCoord2d(1.0, 1.0); glVertex2d(-6.5 + (1.5*i), -1.5);
			}
			glEnd();
			glDisable(GL_TEXTURE_2D);
		glPopMatrix();
		glPushMatrix();
			glLoadIdentity();
			freetype::print(uiFont, 275, 30, "Points: %d", ply->getPoints());
			if (goal->getTimeRequirement() > 0) {
				freetype::print(uiFontSmall, 310, 530, "Bonus Time:");
				freetype::print(uiFont, 325, 475, "%ds", seconds);
			}
		glPopMatrix();
		/* Draw an overlay. */
		glPushMatrix();
			glColor4d(0.75, 0.75, 0.75, 0.5);
			glTranslated(cam.getX(), (cam.getY() + 5.0), 0.0);
			glBegin(GL_TRIANGLE_STRIP);
				glVertex3d(-10.0, -10.0, 1.0);
				glVertex3d(10.0, -10.0, 1.0);
				glVertex3d(-10.0, 10.0, 1.0);
				glVertex3d(10.0, 10.0, 1.0);
			glEnd();
		glPopMatrix();
		glColor3d(0.0, 0.0, 0.0);
		freetype::print(uiFont, 100, 200, "PAUSED");
		freetype::print(uiFontSmall, 125, 150, "Press 'End' to continue.");
		freetype::print(uiFontSmall, 125, 120, "Press 'Esc' to quit.");
		break;
	case HI_SCORES:
		bg.draw();
		freetype::print(uiFont, 100, 300, "Total Points: %d", ply->getPoints());
		freetype::print(uiFontSmall, 100, 250, "Points: %d", ply->getPoints() - seconds);
		freetype::print(uiFontSmall, 500, 250, "Bonus: %d", seconds);
		freetype::print(uiFontSmall, 200, 100, "Press 'ESC' to play again!");
		break;
	case GAME_OVER:
		bg.draw();
		freetype::print(uiFontSmall, 100, 100, "Press 'ESC' to play again!");
		break;
	}
}

bool Game::loadLevel(std::wstring lvlName) {

	std::wstring lvlPath = L"resource\\levels\\";
	std::wifstream lvlFile;
	lvlFile.open(lvlPath.append(lvlName));
	// Do we have a player in the game yet?
	bool player = false;

	int currentTex = 0;
	std::vector<Texture> textures;	// A list of all textures.

	if (!lvlFile.is_open()) {
		_tprintf(L"Could not find level: %s.lev!\n", lvlName);
		return false;
	}

	while (!lvlFile.eof()) {
		std::wstring line;
		std::getline(lvlFile, line);

		std::shared_ptr<StaticEntity> e(nullptr);
		if (std::wstring::npos != line.find(L"(")) {
			int b1 = line.find(L"(");
			int b2 = line.find(L")");
			std::wstring type = line.substr(0, (b1-0));
			if (type == L"bg") {
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				Background bg;
				sstrm >> bg;
				bg.loadTexture(textures.at(currentTex).getTextureID(), textures.at(currentTex).getWidth(), textures.at(currentTex).getHeight());
				std::cout << bg << std::endl;
				e = std::make_shared<Background>(*&bg);
			} else if (type == L"plat") {
				/* Create a platform and make sure it can be stored safely. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				Platform ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID(), textures.at(currentTex).getWidth(), textures.at(currentTex).getHeight());
				std::cout << ent << std::endl;
				e = std::make_shared<Platform>(*&ent);
			} else if (type == L"dplat") {
				/* Create a platform and make sure it can be stored safely. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				DPlatform ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID(), textures.at(currentTex).getWidth(), textures.at(currentTex).getHeight());
				std::cout << ent << std::endl;
				e = std::make_shared<DPlatform>(*&ent);
			} else if (type == L"mvplat") {
				/* Create a moving platform. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				MovingPlatform ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID(), textures.at(currentTex).getWidth(), textures.at(currentTex).getHeight());
				std::cout << ent << std::endl;
				e = std::make_shared<MovingPlatform>(*&ent);
			} else if (type == L"ptpk") {
				/* Create a point pickup. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				PointPickup ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID(), textures.at(currentTex).getWidth(), textures.at(currentTex).getHeight());
				std::cout << ent << std::endl;
				e = std::make_shared<PointPickup>(*&ent);
			} else if (type == L"jumpen") {
				/* Create a point pickup. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				JumpingEnemy ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID());
				std::cout << ent << std::endl;
				e = std::make_shared<JumpingEnemy>(*&ent);
			} else if (type == L"walken") {
				/* Create a point pickup. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				WalkingEnemy ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID());
				std::cout << ent << std::endl;
				e = std::make_shared<WalkingEnemy>(*&ent);
			} else if (type == L"decor") {
				/* Create a point pickup. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				Decoration ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID());
				std::cout << ent << std::endl;
				e = std::make_shared<Decoration>(*&ent);
			} else if (type == L"fdecor") {
				/* Create a point pickup. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				FDecoration ent;
				sstrm >> ent;
				ent.setTexture(textures.at(currentTex).getTextureID());
				std::cout << ent << std::endl;
				e = std::make_shared<FDecoration>(*&ent);
			} else if (type == L"ch") {
				/* Create a point pickup. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				Checkpoint ent;
				sstrm >> ent;
				std::cout << ent << std::endl;
				e = std::make_shared<Checkpoint>(*&ent);
			} else if (type == L"ply") {
				if (!player) {
					/* Create a Player from file. */
					std::wstring vals = line.substr(++b1, (b2-b1));
					std::wstringstream sstrm(vals);
					Player play(ev);
					sstrm >> play;
					play.setTexture(textures.at(currentTex).getTextureID(), textures.at(currentTex).getWidth(), textures.at(currentTex).getHeight());
					ply = std::make_shared<Player>(play);
					player = true;
					std::cout << play << std::endl;
					ents.push_back(ply);
				} else {
					/* A Player already exists, ignore this definition. */
					std::cout << "Player already defined!" << std::endl;
				}
			} else if (type == L"goal") {
				/* Create a game goal. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				GameGoal ent(ev);
				sstrm >> ent;
				std::cout << ent << std::endl;
				goal = std::make_shared<GameGoal>(*&ent);
				e = goal;
			} else if (type == L"spike") {
				/* Create a game goal. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				Spike ent;
				sstrm >> ent;
				std::cout << ent << std::endl;
				e = std::make_shared<Spike>(*&ent);
			} else if (type == L"f") {
				/* Create a game goal. */
				std::wstring vals = line.substr(++b1, (b2-b1));
				std::wstringstream sstrm(vals);
				Follower ent;
				sstrm >> ent;
				std::cout << ent << std::endl;
				e = std::make_shared<Follower>(*&ent);
			} else if (type == L"tex") {
				/* We are looking for a texture now. */
				bool texExists = false;
				std::wstring vals = line.substr(++b1, (b2-b1));
				for (unsigned int i = 0; i < textures.size(); i++) {
					/* Check all loaded textures. */
					Texture storedTex = textures.at(i);
					if (storedTex.getTextureName() == vals) {
						/* If it's already loaded, re-use the ID from map */
						texExists = true;
						currentTex = i;
						std::wcout << L"Texture set (" << vals << L")" 
							<< std::endl;
						break;
					}
				}
				if (!texExists) {
					/* If the texture doesn't exist, add it to the map. */
					Texture t(TGA_ALPHA | TGA_MIPMAPS);
					if (!t.loadTexture(vals)) {
						std::wcerr << L"Failed to load texture! (" << vals << L")"
							<< std::endl;
						t.loadTexture(L"missing");
					} else {
						std::wcout << L"Loaded texture (" << vals << L")" 
							<< std::endl;
					}
					textures.push_back(t);
					currentTex = textures.size() - 1;
				}
			}
		}
		if (e.get() != nullptr) {
			/* As long as the entity is pointed to, add it to the vector. */
			ents.push_back(e);
		}
	}

	lvlFile.close();
	textures.clear();
	std::sort(ents.rbegin(), ents.rend(), TextureCmp());
	return true;
}