#include "include\BaseEnemy.h"
#include "include\Player.h"
#include "include\GameMaths.h"

BaseEnemy::BaseEnemy() {

	solid = true;
	applyGravity = true;
	applyFriction = true;
	/* Set up enemy specific values. */
	health = 1;
	damage = 1;
	startXPos = 0.0;
	startYPos = 0.0;
}

BaseEnemy::BaseEnemy(double x, double y, double w, double h, unsigned char hp, char d) 
	: MovingEntity(x, y, w, h, 0.0, 0.0, 0.0) {

		solid = true;
		applyGravity = true;
		applyFriction = true;
		/* Set up enemy specific values. */
		health = hp;
		damage = d;
		startXPos = x;
		startYPos = y;
}

void BaseEnemy::setTexture(GLuint tex) {

	texID = tex;
	s = 0.0f;
	t = 0.0f;
	u = 1.0f;
	v = 1.0f;
}