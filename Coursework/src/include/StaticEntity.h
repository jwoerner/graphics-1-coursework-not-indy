/*******************************************************************************
 *	Name:			StaticEntity
 *	Author:			Jordan Woerner
 *	Description:	StaticEntity objects hold position and rotation values for 
					the object. These cannot be updated in update(), but can be 
					adjusted post-creation.
 ******************************************************************************/
#ifndef STATIC_ENTITY_H
#define STATIC_ENTITY_H

#include "Polyshape.h"
#include "BaseIncludes.h"
#include <iostream>

/* A collision box offset. */
#define C_OFFSET	0.0
/* Layers for rendering. */
#define BACKGROUND	-0.9
#define BACKDROP	-0.8
#define SCENERY_B	-0.5
#define NORMAL		0.0
#define SCENERY_F	0.5
#define FOREGROUND	1.0

#define PI 3.14159265359

/* Forward declaration for all collidable entities. Used in Visitor Pattern. */
class Platform;
class MovingPlatform;
class PointPickup;
class Player;
class Checkpoint;
class Follower;
class JumpingEnemy;
class WalkingEnemy;
class Spike;
class GameGoal;

class StaticEntity {
protected:
	double xPos;	// X position of the entity.
	double yPos;	// Y position of the entity.
	double rot;		// The rotation of the entity along Z.
	double layer;	// The layer to render this entity to.

	double height;	// The height of this entity.
	double width;	// The width of this entity.
	float u, v, s, t;	// Texture co-ordinates.

	bool solid;		// Can this entity be collided with?
	bool colliding;	// Is this entity currently colliding?

	double mb[16],mb1[16],mb2[16];

	GLuint texID;

public:
	/*
	 StaticEntity()
	 Constructs a new StaticEntity at zero x/y with 1 unit width and height.
	 */
	StaticEntity();
	/*
	 StaticEntity(double, double, double, double, double)
	 Creates a StaticEntity with the specified x/y, width, height and rotation.
	 */
	StaticEntity(double x, double y, double w, double h, double r);

	/*
	 updateVerts()
	 Updates the render and collision area vertices for each entity.
	 */
	virtual void updateVerts();

	/*
	 setPosX(double)
	 Sets the X position of this entity.
	 */
	inline void setXPos(double x) { xPos = x; }
	/*
	 setYPos(double)
	 Sets the Y position of this entity.
	 */
	inline void setYPos(double y) { yPos = y; }
	/*
	 setRot(double)
	 Sets the rotation of this entity.
	 */
	inline void setRot(double r) { rot = r; }
	/*
	 setWidth(double)
	 Sets the width of the entity to the specified value.
	 */
	inline void setWidth(double w) { width = w; }
	/*
	 setHeight(double)
	 Sets the height of the entity to the specified value.
	 */
	inline void setHeight(double h) { height = h; }
	/*
	 setSolid(bool)
	 Sets whether this entity can be collided with or not.
	 */
	inline void setSolid(bool s) { solid = s; }
	/*
	 setColliding(bool)
	 Sets the collision status of this entity.
	 */
	inline void setColliding(bool c) { colliding = c; }
	/*
	 setTexture(GLuint, int, int)
	 Sets the texture this entity will render.
	 */
	void setTexture(GLuint t, int w, int h);
	/*
	 GLuint getTexture() const
	 Gets the currently assigned texture for this entity.
	 */
	inline GLuint getTexture() const { return texID; }

	/*
	 double const getXPos()
	 Returns the X position of this entity.
	 */
	inline double getXPos() const { return xPos; }
	/*
	 double const getYPos()
	 Returns the Y position of this entity.
	 */
	inline double getYPos() const { return yPos; }
	/*
	 double const getRotation()
	 Returns the rotation of this entity.
	 */
	inline double getRotation() const { return rot; }
	/*
	 double const getWidth()
	 Gets the width of this entity as a double.
	 */
	inline double getWidth() const { return width; }
	/*
	 double const getHeight()
	 Gets the height of this entity as a double.
	 */
	inline double getHeight() const { return height; }
	/*
	 double getLayer() const
	 Returns the layer this entity is on.
	 */
	inline double getLayer() const { return layer; }
	/*
	 bool isSolid() const
	 Can this entity be collided with?
	 */
	inline bool isSolid() const { return solid; }
	/*
	 bool isColliding() const
	 Is this entity currently colliding with something?
	 */
	inline bool isColliding() const { return colliding; }

	/*
	 draw()
	 Draws the bounding box for this entity.
	 */
	virtual void draw();
	/*
	 update(double)
	 Updates this entity.
	 */
	virtual void update(double deltaT) { /* Does nothing here. */}

	/*
	 collide(StaticEntity*, Vertex&)
	 Handles collisions between two entities, StaticEntity can be anything that derives StaticEntity.
	 Vertex is the collision vector to use in interaction.
	 Calls the colliding class to work out which response is required using Visitor Pattern.
	 */
	virtual void collide(StaticEntity& ent, Vertex& mtd) { /* No collisions possible. */ }
	/*
	 Collision responses, each entity can call any of these to response to each as needed.
	 */
	virtual void respond(Player& ent, Vertex& mtd) {}
	virtual void respond(Platform& ent, Vertex& mtd) {}
	virtual void respond(MovingPlatform& ent, Vertex& mtd) {}
	virtual void respond(PointPickup& ent, Vertex& mtd) {}
	virtual void respond(WalkingEnemy& ent, Vertex& mtd) {}
	virtual void respond(JumpingEnemy& ent, Vertex& mtd) {}
	virtual void respond(Follower& ent, Vertex& mtd) {}
	virtual void respond(Spike& ent, Vertex& mtd) {}
	virtual void respond(Checkpoint& ent, Vertex& mtd) {}
	virtual void respond(GameGoal& ent, Vertex& mtd) {}
	
	Polyshape verts; // The verticies of this entity.

};

#endif