#include "include\MovingEntity.h"
#include "include\Matrix.h"

MovingEntity::MovingEntity() {

	applyGravity = false;
	applyFriction = false;
	xSpeed = 0.0;
	ySpeed = 0.0;
	velocityX = 0.0;
	velocityY = 0.0;
}

MovingEntity::MovingEntity(double x, double y, double w, double h, double r, double xS, double yS)
	: StaticEntity(x, y, w, h, r) {

	applyGravity = false;
	applyFriction = false;
	xSpeed = xS;
	ySpeed = yS;
	velocityX = 0.0;
	velocityY = 0.0;
}

void MovingEntity::update(double deltaT) {

	/* Smooth the X and Y values with the delta time we passed. */
	xPos += (velocityX * deltaT);
	yPos += (velocityY * deltaT);

	updateVerts();

	/* Calculate gravity if it is enabled. */
	if (applyGravity) {
		if (velocityY >= -5.0)
			velocityY -= 1.0 * deltaT;
		else
			velocityY = -5.0; // Terminal velocity should be changeable.
	}

	/* Decelerate the player if friction is on. */
	if (applyFriction) {
		if (velocityX > 0.01) {
			/* We only need to take half the speed of the entity to slow it */
			velocityX -= (xSpeed*0.95) * deltaT;
		} else if (velocityX < -0.01) {
			velocityX += (xSpeed*0.95) * deltaT;
		} else {
			velocityX = 0.0;
		}
	}
}