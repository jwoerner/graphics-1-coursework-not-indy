#include "include\MenuBackground.h"

MenuBackground::MenuBackground()
{
	texID = 0;
}

void MenuBackground::draw() {

	glBindTexture(GL_TEXTURE_2D, texID);

	glPushMatrix();
		glEnable(GL_TEXTURE_2D); // Enable textures.

		glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2d(0.0, 0.0);
			glVertex3d(-10.0, -10.0, 0.0);
			glTexCoord2d(0.0, 1.0);
			glVertex3d(-10.0, 10.0, 0.0);
			glTexCoord2d(1.0, 0.0);
			glVertex3d(10.0, -10.0, 0.0);
			glTexCoord2d(1.0, 1.0);
			glVertex3d(10.0, 10.0, 0.0);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}