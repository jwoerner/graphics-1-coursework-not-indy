#include "include\Polyshape.h"
#include "include\GameMaths.h"

Polyshape::Polyshape(int num) {

	numVerts = num;
	vert = new Vertex[numVerts];
	cVert = new Vertex[numVerts];
}

Vertex::Vertex() {

	x = 0.0;
	y = 0.0;
}

bool operator==(const Vertex& first, const Vertex& other) {

	if ((first.x == other.x) && (first.y == other.y))
		return true;

	return false;
}

bool operator==(const Vertex& first, const double& other) {

	if ((first.x == other) && (first.y == other))
		return true;

	return false;
}

bool operator>(const Vertex& first, const Vertex& other) {

	double len1 = GameMaths::length(&first);
	double len2 = GameMaths::length(&other);
	
	return (len1 > len2);
}

bool operator>(const Vertex& first, const double& other) {

	double len = GameMaths::length(&first);

	return (len > other);
}