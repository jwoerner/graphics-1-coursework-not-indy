/*******************************************************************************
 *	Name:			GLDisplay
 *	Author:			Jordan Woerner
 *	Description:	GLDisplay is a wrapper for a Win32 window that contains an 
					OpenGL context. The Window can be linked to an 
					"EventHandler" to allow for any Win32 input to be shared
					with other components of the program with ease.
 ******************************************************************************/
#ifndef GLDISPLAY_H
#define GLDISPLAY_H

#include "EventHandler.h"
#include "Camera.h"

class GLDisplay {

public:
	/*
	 GLDisplay(HINSTANCE)
	 Default constructor for an GLDisplay. Accepts the instance handle for this 
	 application as a parameter.
	 */
	GLDisplay(HINSTANCE hInst);
	/*
	 GLDiaply(HINSTANCE, unsigned int, unsigned int)
	 Alternate constructor for a GLDisplay. Accepts the instance handle as with 
	 the default constructor, but also accepts width and height values 
	 (in pixels) for the size of the window.
	 */
	GLDisplay(HINSTANCE hInst, unsigned int w, unsigned int h);

	/*
	 getWidth()
	 Returns the width of this window in pixels as an integer.
	 */
	inline const unsigned int getWidth() {

		return width;	
	}

	/*
	 unsigned int setWidth(unsigned int)
	 Sets the desired width of this window in pixels to the provided unsigned 
	 integer.
	 */
	inline void setWidth(unsigned int w) {

		width = w;
	}
	/*
	 unsigned int getHeight()
	 returns the height of this window in pixels as an integer.
	 */
	inline const unsigned int getHeight() {

		return height;
	}
	/*
	 setHeight(unsigned int)
	 Sets the desired height of this window in pixels to the provided unsigned 
	 integer.
	 */
	inline void setHeight(unsigned int h) {

		height = h;
	}
	/*
	 getDC()
	 Returns the Device Context for this window.
	 */
	inline const HDC getDC() {

		return hDC;
	}

	/*
	 setEventHandler(EventHandler*)
	 Sets the EventHandler for this GLDisplay to the specified pointer.
	 */
	inline void setEventHandler(EventHandler* handler) {

		ev = handler;
	}

	/*
	 False message handler that decides how to deal with a message passed to 
	 this window properly.
	 */
	static LRESULT CALLBACK stWndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar);

	/*
	 bool createGLWindow(TCHAR*)
	 Creates a Win32 window and a valid OpenGL context inside that window. Sets 
	 the title of the window to the provided TCHAR* array.
	 Returns true if the window is created successfully.
	 */
	bool createGLWindow(TCHAR* title);
	/*
	 setVSync(bool)
	 Enables or disables vertical sync of frames for this GLDisplay.
	 */
	void setVSync(bool sync);
	/*
	 resize(unsigned int, unsigned int)
	 Resizes the display safely, and fixes the OpenGL context to match the 
	 resized display.
	 */
	void resize(int w, int h);
	/*
	 update(Camera&)
	 Clears the GLDisplay allowing for updates.
	 */
	void update();

	/*
	 killGLWindow()
	 Safely removes this GLWindow by destroying the OpenGL context, and then the 
	 window itself.
	 */
	void killGLWindow();

private:
	unsigned int width;		// The width of the window in pixels.
	unsigned int height;	// The height of the window in pixels.
	HINSTANCE hInstance;	// The handle for this instance, from Windows.
	HWND hWnd;	// A handle to this window provided by Windows.
	HDC hDC;	// The display context for this window created by Windows.
	HGLRC hRC;	// The rendering context for this window created by OpenGL.

	EventHandler* ev; // The event handler for this GLDisplay.

	/*
	 The real Win32 message handler callback. Called by the false one when a 
	 window message requires it.
	 */
	LRESULT CALLBACK wndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar);
};

#endif