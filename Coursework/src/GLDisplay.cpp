#include "include\BaseIncludes.h"
#include "include\GLDisplay.h"
#include "include\Camera.h"
#include "include\EventHandler.h"
#include <tchar.h>

GLDisplay::GLDisplay(HINSTANCE hInst) {

	GLDisplay::hInstance = hInst;
	GLDisplay::width = 640;
	GLDisplay::height = 480;
}

GLDisplay::GLDisplay(HINSTANCE hInst, unsigned int w, unsigned int h) {

	GLDisplay::hInstance = hInst;
	GLDisplay::width = w;
	GLDisplay::height = h;
}

LRESULT CALLBACK GLDisplay::stWndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar) 
{
	GLDisplay* disp;

	if (msg == WM_NCCREATE) {
		/* If a GLDisplay is created, assign it a long so we can point to it. */
		SetWindowLong(hWnd, GWL_USERDATA, 
			(long)((LPCREATESTRUCT(lPar))->lpCreateParams));
	}
	/* Attempt to get the long that points to this GLDisplay if it exists. */
	disp = (GLDisplay*)GetWindowLong(hWnd, GWL_USERDATA);

	if (disp) {
		/* If this GLDisplay exists, call the real message handler. */
		return disp->wndProc(hWnd, msg, wPar, lPar);
	} else {
		/* Otherwise, we need to create the Windows Process first. */
		return DefWindowProc(hWnd, msg, wPar, lPar);
	}
}

LRESULT CALLBACK GLDisplay::wndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar) {

	/* Handle the messages passed to this. */
	switch (msg) {
	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	case WM_SIZE:
		/* Get the new width and height stored in lParam. */
		resize(LOWORD(lPar), HIWORD(lPar)); // Run the resize function.
		return 0;
	case WM_KEYDOWN:
		/* If the ESC key is pressed, exit the game. */		
		if (wPar == VK_F12) {
			PostQuitMessage(0);
			return 0;
		}
		/* Otherwise just update the pressed key. */
		ev->setKey(wPar, true);
		return 0;
	case WM_KEYUP:
		/* If a key is released, set it to false in the EventManager. */
		ev->setKey(wPar, false);
		return 0;
	case WM_MOUSEMOVE:
		/* When the mouse moves, get the new position. */
		ev->setMouseX(LOWORD(lPar));
		ev->setMouseY(HIWORD(lPar));
		return 0;
	case WM_LBUTTONDOWN:
		ev->setMouseLeft(true);
		return 0;
	case WM_LBUTTONUP:
		ev->setMouseLeft(false);
		return 0;
	case WM_RBUTTONDOWN:
		ev->setMouseRight(true);
		return 0;
	case WM_RBUTTONUP:
		ev->setMouseRight(false);
		return 0;
	}

	return DefWindowProc(hWnd, msg, wPar, lPar);
}

bool GLDisplay::createGLWindow(TCHAR* title) {

	GLuint pixelFormat;
	WNDCLASS wc;
	DWORD dwExStyle;
	DWORD dwStyle;
	RECT windowRect;

	windowRect.left = (long)0;
	windowRect.right = (long)width;
	windowRect.top = (long)0;
	windowRect.bottom = (long)height;

	hInstance = GetModuleHandle(NULL);

	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// The style of the window.
	wc.lpfnWndProc = (WNDPROC)GLDisplay::stWndProc;	// The message handler.
	wc.cbClsExtra = 0;	// How many extra bytes after this struct.
	wc.cbWndExtra = 0;	// How many extra bytes after the window instance.
	wc.hInstance = hInstance;	// The instance handle.
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);		// Which icon to use.
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	// Which cursor to use.
	wc.hbrBackground = NULL;	// The background type for this Window.
	wc.lpszMenuName = NULL;	// The resource name for the class menu.
	wc.lpszClassName = _T("OpenGL");	// A name to reference this class by.

	if (!RegisterClass(&wc)) {
		MessageBox(NULL, _T("Failed to register the window!"), _T("ERROR"),
			MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	dwStyle = WS_OVERLAPPED;

	AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);

	/* Attempt to create the window. */
	if (!(hWnd = CreateWindowEx(dwExStyle,
		_T("OpenGL"),
		title,
		dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		0, 0, // Set the top left corner of the window to 0, 0.
		windowRect.right - windowRect.left, // Set the width.
		windowRect.bottom - windowRect.top, // Set the height.
		NULL,
		NULL,
		hInstance, // The handle for this process.
		(void*)this))) {
			/* If window creation fails at any point, destroy OGL. */
			killGLWindow();
			/* Then show this error message. */
			MessageBox(NULL, _T("Window failed to create!"), _T("ERROR"), 
				MB_OK | MB_ICONEXCLAMATION);
			return false;
	}

	static PIXELFORMATDESCRIPTOR pFD = {
		sizeof(PIXELFORMATDESCRIPTOR), // Size of this struct.
		1, // This should always be 1.
		PFD_DRAW_TO_WINDOW |	// These specify a few things, draws to window.
		PFD_SUPPORT_OPENGL |	// Supports OpenGL drawing.
		PFD_DOUBLEBUFFER,		// Double buffers the drawing.
		PFD_TYPE_RGBA, // Uses the RGBA colour system.
		24, // The number of bits in the colour buffer.
		0, 0, 0, 0, 0, 0, // Number of bits for RGBA bitplanes and shifts.
		0, 
		0, 
		0, 
		0, 0, 0, 0,
		24, // Number of bits for depth buffer. 
		0,	// Number of bits for stencil buffer.
		0,	// Auxiliary buffers (unsupported).
		PFD_MAIN_PLANE,
		0, 
		0, 0, 0
	};

	if (!(hDC=GetDC(hWnd))) {
		killGLWindow();
		MessageBox(NULL, _T("Unable to create a device context!"), _T("ERROR"), 
			MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	if (!(pixelFormat = ChoosePixelFormat(hDC, &pFD))) {
		killGLWindow();
		MessageBox(NULL, _T("Cannot find a matching PixelFormat!"), _T("ERROR"), 
			MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	if (!SetPixelFormat(hDC, pixelFormat, &pFD)) {
		killGLWindow();
		MessageBox(NULL, _T("Cannot set the Pixelformat!"), 
			_T("ERROR"), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	if (!(hRC = wglCreateContext(hDC))) {
		killGLWindow();
		MessageBox(NULL, _T("Cannot create the OpenGL rendering context"), 
			_T("ERROR"), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	if (!wglMakeCurrent(hDC, hRC)) {
		killGLWindow();
		MessageBox(NULL, _T("Cannot activate the OpenGL context!"), _T("ERROR"), 
			MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	ShowWindow(hWnd, SW_SHOW);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	resize(width, height);
	GLfloat beh = (1.0f/255); // Convert RGB values to floats.
	glClearColor(beh*135, beh*206, beh*235, 1.0); // Set the clear colour.
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	resize(width, height);

	GLenum err;
	const GLubyte* errStr;
	while ((err = glGetError()) != GL_NO_ERROR) {
		errStr = gluErrorString(err);
		std::cerr << errStr << std::endl;
	}

	return true; // Window created!
}

void GLDisplay::resize(int w, int h) {

	/* Update the width and height of this display. */
	width = w;
	height = h;

	/* Resize the viewport to match the change in size. */
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-10, 10, -10, 10, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLenum err;
	const GLubyte* errStr;
	while ((err = glGetError()) != GL_NO_ERROR) {
		errStr = gluErrorString(err);
		std::cerr << errStr << std::endl;
	}
}

void GLDisplay::update() {

	/* Work out if the window is currently focused. */
	HWND focus = GetForegroundWindow();
	if (focus != hWnd && ev->getGameState() == PLAYING) {
		/* Pause the game if the window loses focus. */
		ev->setGameState(PAUSE);
		ev->reset();
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/* Load the matrix we stored in the Camera, rather than the identity. */
	glPushMatrix();
	glPopMatrix();
}

void GLDisplay::setVSync(bool sync) {
	// Function pointer for the wgl extention function we need to enable/disable
	// vsync
	typedef BOOL (APIENTRY *PFNWGLSWAPINTERVALPROC)(int);
	PFNWGLSWAPINTERVALPROC wglSwapIntervalEXT = 0;

	const char *extensions = (char*)glGetString( GL_EXTENSIONS );

	if( strstr( extensions,"WGL_EXT_swap_control") == 0 )
	{
		return;
	}
	else
	{
		wglSwapIntervalEXT = (PFNWGLSWAPINTERVALPROC)wglGetProcAddress("wglSwapIntervalEXT");

		if(wglSwapIntervalEXT)
			wglSwapIntervalEXT(sync);
	}
}

void GLDisplay::killGLWindow() {
	
	glDisable(GL_BLEND);

	if (hRC) {
		if (!wglMakeCurrent(NULL, NULL)) {
			/* If we can't remove a GL context, show this error. */
			MessageBox(NULL, _T("DC and RC context failed to release!"), 
				_T("SHUTDOWN ERROR"), MB_OK | MB_ICONINFORMATION);
		}
		if (!wglDeleteContext(hRC)) {
			/* If the rendering context fails to delete, show this error. */
			MessageBox(NULL, _T("Failed to release rendering context!"),
				_T("SHUTDOWN ERROR"), MB_OK | MB_ICONINFORMATION);
		}
		/* It all went as planned, remove the rendering context. */
		hRC = NULL;
	}

	if (hDC && !ReleaseDC(hWnd, hDC)) {
		/* If the device context exists, and we can't remove it, show this. */
		MessageBox(NULL, _T("Device Context failed to release!"),
			_T("SHUTDOWN_ERROR"), MB_OK | MB_ICONINFORMATION);
	}
	if (hWnd && !DestroyWindow(hWnd)) {
		/* If the handle for this window can't be destroyed, show this. */
		MessageBox(NULL, _T("Failed to release hWnd!"), _T("SHUTDOWN_ERROR"), 
			MB_OK | MB_ICONINFORMATION);
	}
	if (!UnregisterClass(_T("OpenGL"), hInstance)) {
		/* If the class fails to unregister, show this error. */
		MessageBox(NULL, _T("Could not unregister class!"), 
			_T("SHUTDOWN_ERROR"), MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;
	}
}