/*******************************************************************************
 *	Name:			Background
 *	Author:			Jordan Woerner
 *	Description:	A static background for the game world.
 ******************************************************************************/
#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "StaticEntity.h"

class Background : public StaticEntity
{
public:
	/*
	 Background()
	 Creates a new Background at zero x/y.
	 */
	Background();
	/*
	 Background(double, double, double, double)
	 Creates a Background at the specified x/y with the provided width 
		and height.
	 */
	Background(double x, double y, double w, double h);

	/*
	 loadTexture(GLuint, int, int, int)
	 Sets the texture for this Background.
	 Texture width and height are provided here too.
	 */
	void loadTexture(GLuint t, int w, int h);

	void draw();

};

std::wistream& operator>>(std::wistream& iStr, Background& b);
std::ostream& operator<<(std::ostream& oStr, const Background& b);

#endif