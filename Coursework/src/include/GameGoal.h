/*******************************************************************************
 *	Name:			GameGoal
 *	Author:			Jordan Woerner
 *	Description:	GameGoals are entities that the player must reach to finish 
					a level, they can have point goals set to stop the player 
					from not collecting items. Or time goals set to give the 
					player a time limit.
 ******************************************************************************/
#ifndef GAME_GOAL_H
#define GAME_GOAL_H

#include "StaticEntity.h"
#include "EventHandler.h"
#include "FreeType.h"
#include <time.h>

class GameGoal : public StaticEntity
{
private:
	EventHandler& ev;		// The EventHandler to switch GameState through.
	unsigned int pointGoal;	// The point requirement of this Goal.
	clock_t timeGoal;		// The time requirement of this Goal.
	clock_t curTime;		// The current time.
	freetype::font_data f;	// Font for drawing the time.

public:
	/*
	 GameGoal(EventHandler&)
	 Creates a new GameGoal using the specified EventHandler to manage the 
		GameState.
	 */
	GameGoal(EventHandler& handler);
	/*
	 GameGoal(EventHandler&, double, double)
	 Creates a new GameGoal using the specified EventHandler to manage the 
		GameState. Accepts two doubles to place the GameGoal in the level.
	 */
	GameGoal(EventHandler& handler, double x, double y);

	/*
	 void setPointRequirement(unsigned int)
	 Sets the point requirement for this GameGoal to the specified integer 
		value. Can be adjusted on the fly.
	 */
	inline void setPointRequirement(unsigned int p) { pointGoal = p; }
	/*
	 unsigned int getPointRequirement() const
	 Gets the point requirement for this GameGoal.
	 */
	inline unsigned int getPointRequirement() const { return pointGoal; }
	/*
	 void setTimeRequirement(unsigned int)
	 Sets the time requirement for this GameGoal to the specified integer value.
	 */
	inline void setTimeRequirement(clock_t t) { timeGoal = t; }
	/*
	 clock_t getTimeRequirement() const
	 Gets the time requirement for this GameGoal as an unsigned integer.
	 */
	inline clock_t getTimeRequirement() const { return timeGoal; }
	/*
	 clock_t getCurrentTime() const
	 Gets the current time this GameGoal is storing.
	 */
	inline clock_t getCurrentTime() const { return curTime; }

	/*
	 void update(double)
	 Updates this GameGoal for time goal tracking, uses the specified double for
		any required smoothing.
	 */
	void update(double deltaT);

	inline void draw() { /* Nothing to draw. */ }

	/*
	 Collision responder.
	 */
	inline void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }

	void respond(Player& ply, Vertex& mtd);
};

/*
 istream& operator>>(wistream&, GameGoal&)
 Creates a GameGoal for the Player to reach. Incorrectly formatted input will 
 cause the GameGoal to fail being created.
 */
std::wistream& operator>>(std::wistream& iStr, GameGoal& g);
/*
 ostream& operator<<(ostream&, GameGoal&)
 Shows information about the GameGoal in the console.
 */
std::ostream& operator<<(std::ostream& oStr, const GameGoal& g);

#endif