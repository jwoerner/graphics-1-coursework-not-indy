#include "include\DebugConsole.h"

#include <windows.h>
#include <tchar.h>
#include <string>
#include <commctrl.h>
#include <commdlg.h>
#include <shellapi.h>
#include <ole2.h>

DebugConsole::DebugConsole() : MAX_ROWS(500), MAX_COLS(80) {

	hStdHandle = INVALID_HANDLE_VALUE;
}

DebugConsole::DebugConsole(int rows, int cols) 
	: MAX_ROWS(rows), MAX_COLS(cols) {

	hStdHandle = INVALID_HANDLE_VALUE;
}

void DebugConsole::open() {

	if (hStdHandle == INVALID_HANDLE_VALUE) {
		CONSOLE_SCREEN_BUFFER_INFO conInfo; // Console details.
		/* Allocate a console for this program. */
		AllocConsole();
		/* Set the screen buffer of the console so we can scroll output. */
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &conInfo);
		conInfo.dwSize.Y = MAX_COLS;
		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), conInfo.dwSize);
		SetConsoleTitle(_T("Debug Console")); // Give it a nice title.

		/* Redirect STDOUT to the console. */
		hStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle((long)hStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		*stdout = *fp;
		setvbuf(stdout, NULL, _IONBF, 0);

		/* Redirect STDIN to the console. */
		hStdHandle = GetStdHandle(STD_INPUT_HANDLE);
		hConHandle = _open_osfhandle((long)hStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "r");
		*stdin = *fp;
		setvbuf(stdout, NULL, _IONBF, 0);

		/* Redirect STDERR to the console. */
		hStdHandle = GetStdHandle(STD_ERROR_HANDLE);
		hConHandle = _open_osfhandle((long)hStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		*stderr = *fp;
		setvbuf(stdout, NULL, _IONBF, 0);
		/* Sync this with every other stdio function. */
		std::ios::sync_with_stdio();
	}
}

void DebugConsole::close() {

	if (hStdHandle != INVALID_HANDLE_VALUE) {
		FreeConsole();
		hStdHandle = INVALID_HANDLE_VALUE;
	}
}

DebugConsole::~DebugConsole() {

	close();
}