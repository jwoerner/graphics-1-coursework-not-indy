/*******************************************************************************
 *	Name:			MovingPlatform
 *	Author:			Jordan Woerner
 *	Description:	A MovingPlatform is based off MovingEntity, allowing it to 
					be affected by gravity and friction. Using provided 
					directions the platform can move up, down, left and right at
					 a specified speed for a specified distance before it 
					reverses direction.
 ******************************************************************************/
#ifndef MOVING_PLATFORM_H
#define MOVING_PLATFORM_H

#include "MovingEntity.h"
#include <iostream>
#include "Player.h"

class MovingPlatform : public MovingEntity {
private:
	double startXPos;	// The starting position to calculate distance on X.
	double startYPos;	// The starting position to calculate distance on Y.
	Directions dir;		// The direction to move in (LEFT, RIGHT, UP, DOWN).
	double distance;	// The distance to move.
	bool reversed;		// Is the platform going backwards?
	double timer;		// Times how long the platform has been still.
	
public:
	/*
	 MovingPlatform()
	 Creates a MovingPlatform in the default location with no attributes.
	 */
	MovingPlatform();
	/*
	 MovingPlatform(double, double, double, double, double, Directions, double, double)
	 Creates a MovingPlatform. Parameters are; x position, y position, width, height, 
		rotation, direction, movement distance and speed.
	 */
	MovingPlatform(double x, double y, double w, double h, double r,
		Directions d, double dist, double spd);

	/*
	 setDirection(Directions)
	 Sets the direction this MovingPlatform should move in.
	 */
	inline void setDirection(Directions d) { dir = d; }
	/*
	 setDistance(double)
	 Sets the distance this MovingPlatform should move.
	 */
	inline void setDistance(double dist) { distance = dist; }

	/*
	 Directions getDirection() const
	 Gets the direction this MovingPlatform moves in.
	 */
	inline Directions getDirection() const { return dir; }
	/*
	 double getDistance() const
	 Gets the distance this MovingPlatform moves.
	 */
	inline double getDistance() const { return distance; }

	/*
	 reverse()
	 Reverses the direction of this MovingPlatform.
	 */
	void reverse();

	void update(double detlaT);

	/*
	 Visitor call.
	 */
	inline void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }
	
	/*
	 Collision handlers.
	 */
	void respond(MovingPlatform& ent, Vertex& mtd);
};

std::wistream& operator>>(std::wistream& iStr, MovingPlatform& p);

std::ostream& operator<<(std::ostream& oStr, const MovingPlatform& p);

#endif