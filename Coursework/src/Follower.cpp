#include "include\Follower.h"
#include "include\Player.h"

Follower::Follower() 
	: MovingEntity(0.0, 0.0, 1.5, 4.0, 0.0, 2.5, 0.0) {

		health = 1;
		solid = true;
		following = false;
		pos = 0;
		ply = NULL;
}

Follower::Follower(double x, double y, unsigned char h) 
	: MovingEntity(x, y, 1.5, 4.0, 0.0, 2.5, 0.0) {

		health = h;
		solid = true;
		following = false;
		pos = 0;
		ply = NULL;
}

void Follower::update(double deltaT) {

	if (ply == NULL)
		return;

	velocityX = ply->getXVelocity();
	velocityY = ply->getYVelocity();

	MovingEntity::update(deltaT);
}

std::wistream& operator>>(std::wistream& in, Follower& f) {

	double x, y;
	int hp;
	wchar_t c1, c2;

	if (in >> x >> c1 >> y >> c2 >> hp) {
		if (c1 == ',' && c2 == ',') {
			f.setXPos(x);
			f.setYPos(y);
			f.setHealth(hp);
			f.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "Follower input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const Follower& f) {

	out << "f(" << f.getXPos() << ", " << f.getYPos() << ", " << f.getHealth() << ")";
	return out;
}

void Follower::respond(Player& ent, Vertex& mtd) {
	
	if (!following) {
		colliding = true;
		following = true;
		ply = &ent;
	}
}

void Follower::respond(Platform& ent, Vertex& mtd) {

	velocityY = 0.0;
	yPos += mtd.y;
}