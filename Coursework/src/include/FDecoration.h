/*******************************************************************************
 *	Name:			FDecoration
 *	Author:			Jordan Woerner
 *	Description:	FDecorations are foreground decorations. Their 
					implementation is basically the same, just this sets a 
					different layer.
 ******************************************************************************/
#ifndef F_DECORATION_H
#define F_DECORATION_H

#include "Decoration.h"

class FDecoration :	public Decoration
{
public:
	/*
	 FDecoration()
	 Creates a new FDecoration at zero x/y with width and height, no rotation.
	 */
	FDecoration();
	/*
	 FDecoration()
	 Creates a new FDecoration at the specified x/y with the provided width, 
		height and rotation.
	 */
	FDecoration(double x, double y, double w, double h, double r);
};

std::wistream& operator >>(std::wistream& in, FDecoration& d);
std::ostream& operator <<(std::ostream& out, const FDecoration& d);

#endif