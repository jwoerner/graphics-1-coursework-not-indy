#include "include\BaseIncludes.h"
#include "include\GLDisplay.h"
#include "include\EventHandler.h"
#include "include\Camera.h"
#include "include\Game.h"
#include "include\DebugConsole.h"

#include <tchar.h>

/**
* WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
* The main entry point for this Win32 application. Creates an OpenGL enabled 
* window, handles input and game logic loop.
*/
int WINAPI WinMain(HINSTANCE hInstance, 
	HINSTANCE hPrevInstance, 
	LPSTR lpCmdLine,
	int nCmdShow) {
		
		EventHandler ev;
		/* Initialise and create the OpenGL window and context. */
		GLDisplay* disp = new GLDisplay(hInstance, 800, 600);
		disp->createGLWindow(_T("Not-Indy! (Press F12 to quit)"));
		disp->setEventHandler(&ev);

		/* Check if a display has been created or not. */
		bool done = disp ? false : true;

		if (!done) {
			/* If there is a display, we need to run the game. */
			MSG msg;
			disp->setVSync(false);
			#ifdef _DEBUG
			/* If we are debugging the game, open the console. */
			DebugConsole con;
			con.open(); // Open the console first;
			#endif			
			DWORD prevTime = GetTickCount(); // Store the starting time.

			/* Now we can start the game. */
			Game game(ev);
			game.initMainMenu();
			std::cout << "game started\n";
			while (!done) {
				while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
					/* If a message is sent, handle it appropriately. */
					if (msg.message == WM_QUIT) {
						/* If the message is "quit", kill the window. */
						disp->killGLWindow();
						done = true;
					} else {
						/* For anything else, let the window handle it. */
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
				/* Then we update the game */
				DWORD currentTime = GetTickCount();
				double deltaT = (currentTime - prevTime) * 0.001; // Time in sec
				/* Update stored time to current time to correct delta. */
				prevTime = currentTime;
				/* 
				GetTickCount sometimes does not have good resolution; 
				sometimes as low as to the nearest 16ms.
				So sometimes deltaT is 0. This can be bad.
				Stepping by 0 can result in bad physics problems, so if the 
				time is <1ms, clamp to 1ms. 
				*/
				if (deltaT < 0.01) {
					deltaT = 0.01;
				}
				/* We shouldn't check collisions on the first update, some 
					things need one update to position themselves.*/
				//if (!game.isFirstUpdate()) {
					game.checkCollisions();
				//}
				game.update(deltaT); // Update the game using delta frame time.
				/* First we clear the screen. */
				disp->update();
				/* Now we can render the game. */
				game.display();
				/* Check for any OpenGL errors post-frame. */
				GLenum err;
				const GLubyte* errStr;
				while (!done && (err = glGetError()) != GL_NO_ERROR) {
					errStr = gluErrorString(err);
					std::cerr << errStr << std::endl;
				}
				SwapBuffers(disp->getDC()); // Swap buffers.
			}
			/* As a window existed, let Windows deal with the exit code. */
			delete disp; // Delete the GLDisplay pointer.
			#ifdef _DEBUG
			con.close();
			#endif
			return (int)(msg.wParam);
		} else {
			/* Show this just in case the display fails to start. */
			MessageBox(NULL, L"Failed to initialise display!", L"Critical Error!", MB_OK | MB_ICONERROR);
		}
		/* If the display creation fails, just exit with this error code. */
		delete disp;
		return -1;
}