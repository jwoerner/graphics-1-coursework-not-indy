/*******************************************************************************
 *	Name:			Camera
 *	Author:			Jordan Woerner
 *	Description:	Cameras can focus on entities, or be free-floating and user 
					directed, any movement of a tracked entity or from user 
					input causes a co-ordinate system translation, applied once 
					everything has rendered.
 ******************************************************************************/
#ifndef CAMERA_H
#define CAMERA_H

#include "MovingEntity.h"
#include "EventHandler.h"

class Camera {
private:
	EventHandler& ev;	// The EventHandler to use for input.
	MovingEntity* e;	// The entity this camera should follow.

	double camX;	// The X position of this camera.
	double camY;	// The Y position of this camera.
	double zoom;	// The zoom of this camera.

public:
	/*
	 Camera(EventHandler&)
	 Creates a camera at zero x/y.
	 */
	Camera(EventHandler& evH);
	/*
	 Camera(EventHandler, double, double)
	 Creates a camera at the specified x/y.
	 */
	Camera(EventHandler& evH, double x, double y);

	/*
	 double getX() const
	 Gets the X position of this camera.
	 */
	inline double getX() const { return camX; }
	/*
	 double getY() const
	 Gets the Y position of this camera.
	 */
	inline double getY() const { return camY; }

	/*
	 setZoom(double)
	 Sets the zoom value for this camera (between 1 and 5)
	 */
	inline void setZoom(double z) {

		if (z <= 5.0 && z >= 1.0)
			zoom = z;
	}
	/*
	 follow(MovingEntity*)
	 Tells this camera which entity to follow.
	 */
	void follow(MovingEntity* ent);
	/*
	 reset()
	 Resets this camera to 0 x/y and zoom.
	 */
	inline void reset() {

		camX = 0.0;
		camY = 0.0;
		zoom = 1.0;
	}

	/*
	 update(double)
	 Updates this camera, accepting input and changes in position of followed 
		entity.
	 */
	void update(double deltaT);

};

#endif