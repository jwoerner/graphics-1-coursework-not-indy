#include "include\Camera.h"
#include "include\BaseIncludes.h"

Camera::Camera(EventHandler& evH) : ev(evH) {

	camX = 0.0;
	camY = 0.0;
	zoom = 1.0;
	e = NULL;
}

Camera::Camera(EventHandler& evH, double x, double y) : ev(evH) {

	camX = x;
	camY = y;
	zoom = 1.0;
	e = NULL;
}

void Camera::follow(MovingEntity* ent) {

	if (ent != NULL) {
		e = ent; // Set the entity to follow.
		/* Then reset the camera position. */
		camX = e->getXPos();
		camY = e->getYPos();
	}
}

void Camera::update(double deltaT) {

	/* As long as something is pointed to by this Camera. */
	if (e != NULL) {
		/* Update the X position when it moves a bit too far.
			If we use the entity velocity we can follow it properly. */
		if (fabs(camX - e->getXPos()) >= 2.5)
			camX += e->getXVelocity() * deltaT;
		
		if (fabs(camX - e->getXPos()) >= 5.0)
			camX = e->getXPos();
		
		/* And do the same for Y position of the Camera. */
		if (fabs(camY - e->getYPos()) >= 0.1)
			camY = e->getYPos();
	} else {
		/* If the camera isn't following anything, set it to free mode. */
		if (ev.getKey(VK_NUMPAD2)) {
			/* Move the camera up. */
			camY -= 2.5 * deltaT;
		}
		if (ev.getKey(VK_NUMPAD4)) {
			/* Move the camera left. */
			camX -= 2.5 * deltaT;
		}
		if (ev.getKey(VK_NUMPAD6)) {
			/* Move the camera right. */
			camX += 2.5 * deltaT;
		}
		if (ev.getKey(VK_NUMPAD8)) {
			/* Move the camera down. */
			camY += 2.5 * deltaT;
		}
	}
	/* Allow these to happen no matter what the camera is doing. */
	if (ev.getKey(VK_OEM_PLUS)) {
		/* Zoom the camera in. */
		zoom += 1.0 * deltaT;
	}
	if (ev.getKey(VK_OEM_MINUS)) {
		/* Zoom the camera out. Ensure it can't go below one. */
		if (zoom > 1.0) {
			zoom -= 1.0 * deltaT;
		}
	}
	if (ev.getKey(VK_NUMPAD5)) {
		/* Reset the camera. */
		camX = 0.0;
		camY = 0.0;
		zoom = 1.0;
	}

	/* Adjust the rendering matrix based on this camera. */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	/* Scale everything appropriately. */
	glScaled(zoom, zoom, 1.0);
	/* Translate everything into position (add extra to Y). */
	glTranslated(-camX, -camY-5.0, 0.0);
}