#include "include\Spike.h"
#include "include\Player.h"
#include "include\Texture.h"

Spike::Spike() {

	Texture t(TGA_ALPHA | TGA_MIPMAPS);
	t.loadTexture(L"spike");
	texID = t.getTextureID();
	height = 0.5;
	width = 0.5;
	damage = 0;
	layer = SCENERY_F;
	solid = true;
}

Spike::Spike(double x, double y, double w, double h, short dam) 
	: StaticEntity(x, y, w, h, 0.0) {

		Texture t(TGA_ALPHA | TGA_MIPMAPS);
		t.loadTexture(L"spike");
		texID = t.getTextureID();
		height = 0.5;
		damage = dam;
		layer = SCENERY_F;
		solid = true;
}

void Spike::updateVerts() {

	StaticEntity::updateVerts();
	u = (float)width * 2;
	v = 1.0;
}

std::wistream& operator>>(std::wistream& in, Spike& s) {
	
	double x, y, w;
	short d;
	wchar_t c1, c2, c3;

	if(in >> x >> c1 >> y >> c2 >> w >> c3 >> d) {
		if (c1 == ',' && c2 == ',' && c3 == ',') {
			s.setXPos(x);
			s.setYPos(y);
			s.setWidth(w);
			s.setDamage(d);
			s.updateVerts();
		} else {
			in.setstate(std::ios::failbit);
			std::cerr << "Gamegoal not correctly formed!" << std::endl;
		}
	}

	return in;
}

std::ostream& operator<<(std::ostream& out, const Spike& s) {

	out << "spike(" << s.getXPos() << ", " << s.getYPos() << ", " << s.getWidth();
	out << ", " << s.getDamage() << ")";

	return out;
}

void Spike::respond(Player& ent, Vertex& mtd) {

	if (!ent.isInvuln()) {
		ent.modifyHealth(-damage);
		ent.respawn();
	}
}