#include "include\Collision.h"
#include <math.h>
#include <iostream>
#include "include\GameMaths.h"

Vertex calcMTD(Polyshape* a, Polyshape* b) {

	Vertex v;
	v.x = 0.0; v.y = 0.0;

	Vertex* axis1 = new Vertex[a->numVerts];
	Vertex* axis2 = new Vertex[b->numVerts];
	Vertex smallest;
	double len, overlap = 99999;
	double* shape1 = new double[2];
	double* shape2 = new double[2];

	/* Calculate all the possible axes for the first shape. */
	for (int side = 0; side < a->numVerts; side++) {
		if (side == 0) {
			axis1[0].x = a->cVert[a->numVerts - 1].y - a->cVert[0].y;
			axis1[0].y = a->cVert[0].x - a->cVert[a->numVerts - 1].x;
		} else {
			axis1[side].x = a->cVert[side - 1].y - a->cVert[side].y;
			axis1[side].y = a->cVert[side].x - a->cVert[side - 1].x;
		}
	}
	/* Calculate all the axes for the second shape. */
	for (int side = 0; side < b->numVerts; side++) {
		if (side == 0) {
			axis2[0].x = b->cVert[b->numVerts - 1].y - b->cVert[0].y;
			axis2[0].y = b->cVert[0].x - b->cVert[b->numVerts - 1].x;
		} else {
			axis2[side].x = b->cVert[side - 1].y - b->cVert[side].y;
			axis2[side].y = b->cVert[side].x - b->cVert[side - 1].x;
		}
	}

	/* Check both shapes against all the axes of the first shape. */
	for (int i = 0; i < a->numVerts; i++) {
		/* Normalise the axis we are working with this iteration. */
		len = GameMaths::length(&axis1[i]);
		axis1[i].x/=len; axis1[i].y/=len;  // Unit vector.

		/* Project the shapes onto the axis, store the result in an array.
			(0 = min, 1 = max) */
		projectShape(shape1, axis1[i], a);
		projectShape(shape2, axis1[i], b);

		/* Perform the SAT test to see if the shapes are colliding. */
		if (!(shape1[0] > shape2[1] || shape2[0] > shape1[1])) {
			/* If they are, get the overlap. */
			double depth = min(shape1[1], shape2[1]) - max(shape1[0], shape2[0]);
			if (depth < overlap) {
				/* If this is the smallest overlap thus far, awesome, use it. */
				overlap = depth;
				smallest = axis1[i];
			}
		} else {
			/* If we hit this, the shapes cannot be overlapping at all. */
			delete[] shape1;
			delete[] shape2;
			delete[] axis1;
			delete[] axis2;

			return v;
		}
	}
	/* Do the same for all the axes of the second shape. */
	for (int i = 0; i < b->numVerts; i++) {
		len = GameMaths::length(&axis2[i]);
		axis2[i].x/=len; axis2[i].y/=len;

		projectShape(shape1, axis2[i], a);
		projectShape(shape2, axis2[i], b);

		/* Do the SAT test again to see if the shapes are colliding. */
		if (!(shape1[0] > shape2[1] || shape2[0] > shape1[1])) {
			double depth = min(shape1[1], shape2[1]) - max(shape1[0], shape2[0]);
			if (depth < overlap) {
				/* If this is the smallest overlap thus far, awesome, use it. */
				overlap = depth;
				smallest = axis2[i];
			}
		} else {
			/* If we hit this, the shapes cannot be overlapping at all. */
			delete[] shape1;
			delete[] shape2;
			delete[] axis1;
			delete[] axis2;

			return v;
		}
	}
	/* Work out if we need to flip the normal. */
	Vertex dist;
	/* Take the distance between the centre of each Polyshape. */
	dist.x = b->center.x - a->center.x;
	dist.y = b->center.y - a->center.y;
	double d = GameMaths::dotProduct(&dist, &smallest); // Dot product it.
	if (d < 0.0) {
		/* If it's less than zero, we need to invert the normals. */
		smallest.x *= -1.0;
		smallest.y *= -1.0;
	}
	
	/* Store the smallest overlap multiplied by the axis it was on.
		Due to normalisation, one of these will be zero. Hopefully. */
	v.x = (overlap * smallest.x);
	v.y = (overlap * smallest.y);

	delete[] shape1;
	delete[] shape2;
	delete[] axis1;
	delete[] axis2;

	return v;
}

void projectShape(double* minMax, Vertex& axis, Polyshape* p) {

	/* Hold the dox product of the shape on the axis. */
	double tmp = GameMaths::dotProduct(&axis, &p->cVert[0]);
	minMax[0] = minMax[1] = tmp; // Use it as the starting value.

	/* Check each vertex projection against the min and max. */
	for (int i = 1; i < p->numVerts; i++) {
		tmp = GameMaths::dotProduct(&p->cVert[i], &axis);
		if (tmp > minMax[1])
			/* Store a new max. */
			minMax[1] = tmp;
		else if (tmp < minMax[0])
			/* Store a new min. */
			minMax[0] = tmp;
	}
}