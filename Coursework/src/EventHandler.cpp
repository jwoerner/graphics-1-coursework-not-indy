#include "include\EventHandler.h"

EventHandler::EventHandler() {

	/* Initialise the keys array properly. All keys default to false. */
	for (int i = 0; i < 256; i++) {
		EventHandler::keys[i] = false;
	}
	EventHandler::mouseLeft = false;
	EventHandler::mouseRight = false;
	EventHandler::mouseX = 0;
	EventHandler::mouseY = 0;
}