#include "include\Background.h"

Background::Background() {

	solid = false;
	layer = BACKGROUND;
}

Background::Background(double x, double y, double w, double h) 
	: StaticEntity(x, y, w, h, 0.0) {

	solid = false;
	layer = BACKGROUND;
}

void Background::loadTexture(GLuint t, int w, int h) {

	setTexture(t, w, h);
	updateVerts();
}

void Background::draw() {

	glPushMatrix();
		glEnable(GL_TEXTURE_2D); // Enable textures.

		glBegin(GL_QUADS);
			glTexCoord2d(s, v);
			glVertex3d(verts.vert[3].x, verts.vert[3].y, layer);
			glTexCoord2d(s, t);
			glVertex3d(verts.vert[0].x, verts.vert[0].y, layer);
			glTexCoord2d(u, t);
			glVertex3d(verts.vert[1].x, verts.vert[1].y, layer);
			glTexCoord2d(u, v);
			glVertex3d(verts.vert[2].x, verts.vert[2].y, layer);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

std::wistream& operator>>(std::wistream& iStr, Background& b) {

	double x, y, w, h;
	wchar_t c1, c2, c3;

	if (iStr >> x >> c1 >> y >> c2 >> w >> c3 >> h) {
		if (c1 == ',' && c2 == ',' && c3 == ',') {
			b.setXPos(x);
			b.setYPos(y);
			b.setWidth(w);
			b.setHeight(h);
			b.updateVerts();
		} else {
			iStr.setstate(std::ios_base::failbit);
			std::cerr << "Platform input not properly formed!" 
				<< std::endl;
		}
	}
	return iStr;
}

std::ostream& operator<<(std::ostream& oStr, const Background& b) {

	oStr << "bg(" << b.getXPos() << ", " << b.getYPos() << ")";
	return oStr;
}