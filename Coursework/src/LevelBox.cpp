#include "include\LevelBox.h"


LevelBox::LevelBox(void) {

	lvlName;
	number = 0;
	texID = 0;
}

LevelBox::LevelBox(std::wstring name, unsigned int n)
{
	lvlName = name;
	number = n;
	texID = 0;
}

void LevelBox::draw() {

	if (texID != 0) {
		glBindTexture(GL_TEXTURE_2D, texID);
		glEnable(GL_TEXTURE_2D);
	}
	glPushMatrix();
		glTranslated(2.5*(3%(6/number)), 0.0, 0.0);
		glColor3d(1.0, 1.0, 1.0);
		glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2d(0.0, 0.0);
			glVertex3d(-9.5, -3.5, 0.2);
			glTexCoord2d(0.0, 1.0);
			glVertex3d(-9.5, 2.5, 0.2);
			glTexCoord2d(1.0, 0.0);
			glVertex3d(-4.5, -3.5, 0.2);
			glTexCoord2d(1.0, 1.0);
			glVertex3d(-4.5, 2.5, 0.2);
		glEnd();

	glPopMatrix();

	if (texID != 0)
		glDisable(GL_TEXTURE_2D);
}
