/*******************************************************************************
 *	Name:			Checkpoint
 *	Author:			Jordan Woerner
 *	Description:	Checkpoints set the Player respawn point to the position of 
					this checkpoint.
 ******************************************************************************/
#ifndef CHECKPOINT_H
#define CHECKPOINT_H

#include "StaticEntity.h"

class Checkpoint : public StaticEntity
{
private:
	bool triggered;	// Has this checkpoint been triggered?

public:
	/*
	 Checkpoint()
	 Creates a checkpoint at zero x/y.
	 */
	Checkpoint();
	/*
	 Checkpoint(double, double)
	 Creates a checkpoint at the specified x and y.
	 */
	Checkpoint(double x, double y);

	/*
	 trigger()
	 Triggers this checkpoint, stopping it from woking again.
	 */
	inline void trigger() { triggered = true; }
	/*
	 bool isTriggered() const
	 Returns true if this checkpoint has been triggered, false otherwise.
	 */
	inline bool isTriggered() const { return triggered; }

	void draw();

	/*
	 Collision handler.
	 */
	inline void collide(StaticEntity& ent, Vertex& mtd) { if (!triggered) ent.respond(*this, mtd); }
	/*
	 Collision response.
	 */
	void respond(Player& ply, Vertex& mtd);
};

std::wistream& operator>>(std::wistream& in, Checkpoint& c);
std::ostream& operator<<(std::ostream& out, const Checkpoint& c);

#endif