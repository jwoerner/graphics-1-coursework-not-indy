/*******************************************************************************
 *	Name:			Collision
 *	Author:			Jordan Woerner
 *	Description:	Collision detection methods.
 ******************************************************************************/
#ifndef COLLISION_H
#define COLLISION_H

#include "Polyshape.h"

/*
 Vertex calcMTD(Polyshape*, Polyshape*)
 Calculates the minimal translational distance to respond to the collision.
 A vertex of translation distances.
 */
Vertex calcMTD(Polyshape* p1, Polyshape* p2);

/*
 double* projectShape(Vertex&, Polyshape*)
 Projects a Polyshape across a provided set of axes.
 Returns an array of double values; 0 = min, 1 = max.
 */
void projectShape(double* minMax, Vertex& axis, Polyshape* p1);

#endif