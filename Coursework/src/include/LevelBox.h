/*******************************************************************************
 *	Name:			LevelBox
 *	Author:			Jordan Woerner
 *	Description:	LevelBox is a menu system entity that displays a level and 
					stores the name of the level so it can be loaded.
 ******************************************************************************/
#ifndef LEVEL_BOX_H
#define LEVEL_BOX_H

#include "BaseIncludes.h"

class LevelBox
{
private:
	std::wstring lvlName;
	unsigned int number;
	GLuint texID;

public:
	/*
	 LevelBox()
	 Creates a new LevelBox with no stored level.
	 */
	LevelBox();
	/*
	 LevelBox(std::wstring, unsigned int)
	 Creates a new LevelBox with a stored level.
	 */
	LevelBox(std::wstring name, unsigned int n);

	/*
	 setLevel(std::wstring)
	 Sets the level this LevelBox stores.
	 */
	inline void setLevel(std::wstring name) { lvlName = name; }
	/*
	 std::wstring getLevel() const
	 Gets the name of the level this LevelBox is storing.
	 */
	inline std::wstring getLevel() const { return lvlName; }

	/*
	 setTexture(GLuint)
	 Sets the texture this box will use to render.
	 */
	inline void setTexture(GLuint t) { texID = t; }
	/*
	 GLuint getTexture() const
	 Gets the reference ID for the texture this LevelBox is using.
	 */
	inline GLuint getTexture() const { return texID; }

	/*
	 draw()
	 Draws this LevelBox to the screen.
	 */
	void draw();
};

#endif