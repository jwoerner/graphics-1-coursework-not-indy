/*******************************************************************************
 *	Name:			Platform
 *	Author:			Jordan Woerner
 *	Description:	A Platform is a static Platform in the game world that does 
					not move from the location it has been given.
 ******************************************************************************/
#ifndef PLATFORM_H
#define PLATFORM_H

#include "StaticEntity.h"

class Platform : public StaticEntity
{
public:
	Platform();
	/*
	 Platform(double, double, double, double)
	 Creates a new Platform at the specified point with the specified size. 
	 Arguments are; X position, Y position, width, height.
	 */
	Platform(double x, double y, double w, double h, double rot);

	/*
	 Collision handler.
	 */	
	void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }

};

/*
 istream& operator>>(wistream&, Platform&)
 Creates a Platform using input streams. Incorrectly formatted input 
 causes the Platform creation to fail.
 */
std::wistream& operator>>(std::wistream& iStr, Platform& p);
/*
 ostream& operator<<(ostream&, Platform&)
 Prints out details about this Platform to the console.
 */
std::ostream& operator<<(std::ostream& oStr, const Platform& p);

#endif