/*******************************************************************************
 *	Name:			Decoration
 *	Author:			Jordan Woerner
 *	Description:	Decorations are non-solid entities with a texture to place 
					in the game world to stop it being so boring.
 ******************************************************************************/
#ifndef DECORATION_H
#define DECORATION_H

#include "StaticEntity.h"
#include "BaseIncludes.h"

class Decoration :	public StaticEntity
{
public:
	/*
	 Decoration()
	 Creates a new decoration with the default values.
	 */
	Decoration();
	/*
	 Decoration(double, double, double, double)
	 Creates a new Decoration at the specified x/y with the specified width and height.
	 */
	Decoration(double x, double y, double w, double h, double r);

	/*
	 setTexture(GLuint)
	 Sets the texture of this Decoration (sets u/v to 1.0 rather than scaling).
	 */
	void setTexture(GLuint t);
};

std::wistream& operator >>(std::wistream& in, Decoration& d);
std::ostream& operator <<(std::ostream& out, const Decoration& d);

#endif