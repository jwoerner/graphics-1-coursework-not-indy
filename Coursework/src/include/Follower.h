/*******************************************************************************
 *	Name:			Follower
 *	Author:			Jordan Woerner
 *	Description:	Follower entities follow a specified player.
 ******************************************************************************/
#ifndef FOLLOWER_H
#define FOLLOWER_H

#include "MovingEntity.h"

class Follower : public MovingEntity
{
private:
	bool following;			// Is this Follower following?
	Vertex move;			// How much should the follower move by?
	unsigned char health;	// How much health does this follower have?
	unsigned char pos;		// How far back should this follower be?
	Player* ply;			// Who is the Follower following?

public:
	/*
	 Follower()
	 Creates a new Follower at zero x/y. 1 health and not following.
	 */
	Follower();
	/*
	 Follower(double, double, unsigned char)
	 Creates a new Follower at the specified x/y with the specified health.
	 */
	Follower(double x, double y, unsigned char h);

	/*
	 setFollowing(bool)
	 Sets whether this Follower is following anything.
	 */
	inline void setFollowing(bool follow) { following = follow; }
	/* 
	 bool isFollowing() const
	 Returns true if the Follower is following something. False otherwise.
	 */
	inline bool isFollowing() const { return following; }
	/*
	 setHealth(char)
	 Sets the health of this Follower.
	 */
	inline void setHealth(char h) { health = h; }
	/*
	 modifyHealth(char)
	 Modifies the health of this Follower.
	 */
	inline void modifyHealth(char h) { health += h; }
	/*
	 unsigned char getHealth() const
	 Gets the health of this follower.
	 */
	inline unsigned char getHealth() const { return health; }
	/*
	 setPos(unsigned char)
	 Sets the position of this Follower in the queue.
	 */
	inline void setPos(unsigned char p) { pos = p; }
	/*
	 unsigned char getPos() const
	 Gets the position of this Follower in the queue.
	 */
	inline unsigned char getPos() const { return pos; }
	/*
	 setMove(Vertex)
	 Sets the next move this Follower should make.
	 */
	inline void setMove(Vertex v) { move = v; }

	void update(double deltaT);

	inline void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }

	void respond(Player& ent, Vertex& mtd);
	void respond(Platform& ent, Vertex& mtd);
};

std::wistream& operator>>(std::wistream& in, Follower& f);
std::ostream& operator<<(std::ostream& out, const Follower& f);

#endif