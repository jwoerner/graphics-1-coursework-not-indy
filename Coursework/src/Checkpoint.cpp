#include "include\Checkpoint.h"
#include "include\Player.h"

Checkpoint::Checkpoint() 
	: StaticEntity(0.0, 0.0, 1.0, 1.0, 0.0) {

	triggered = false;
	solid = true;
}

Checkpoint::Checkpoint(double x, double y) 
	: StaticEntity(x, y, 1.0, 1.0, 0.0) { 
	
		triggered = false;
		solid = true;
}

void Checkpoint::draw() { /* Nothing to draw. */ }

void Checkpoint::respond(Player& ply, Vertex& mtd) {

	ply.setSpawnPoint(xPos, yPos);
	triggered = true;
}

std::wistream& operator>>(std::wistream& in, Checkpoint& c) {

	double x, y;
	wchar_t c1;

	if (in >> x >> c1 >> y ) {
		if (c1 == ',') {
			c.setXPos(x);
			c.setYPos(y);
			c.updateVerts();
		} else {
			in.setstate(std::ios_base::failbit);
			std::cerr << "Checkpoint input not properly formed!" 
				<< std::endl;
		}
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const Checkpoint& c) {

	out << "ch(" << c.getXPos() << ", " << c.getYPos() << ")";
	return out;
}