/*******************************************************************************
 *	Name:			Player
 *	Author:			Jordan Woerner
 *	Description:	Player is the player object. It responds to user input, 
					moving around the world and interacting with the world. It 
					requires an EventHandler to be connected to it to be usable.
 ******************************************************************************/
#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <time.h>
#include "MovingEntity.h"
#include "EventHandler.h"

class Player : public MovingEntity
{
private:
	EventHandler& ev;			// We need some way to get the user input.
	unsigned char maxHealth;	// The maximum health of the player.
	unsigned char health;		// The current health of the player.
	unsigned char lives;		// The number of retries the player has.
	unsigned int totalPoints;	// The total points the player has earned.
	bool invuln;				// Is the play invincible?
	clock_t fallStart;			// When the player started falling.
	clock_t invulnStart;		// When the player became invicible.

	float aniOffset;		// An offset for animaton frames.	
	clock_t curFrame;		// The current frameTime of animation.
	unsigned int frameRate;	// The speed the frames should run at.
	unsigned short frames;	// How many frames are in an animation.
	unsigned short frame;	// The current frame.
	bool aniRev;			// Is the animation running backwards?
	bool faceLeft;			// Is the player facing left?
	Directions moveDir;		// Which direction is the player moving?

	Vertex spawnPos;	// Where should the player respawn?

public:
	/*
	 Player(EventHandler&)
	 Create a new Player and use the specified EventHandler for user input.
	 */
	Player(EventHandler& handler);
	/*
	 Player(EventHandler&, int, int)
	 Create a new Player and use the specified EventHandler for user input.
	 Allows number of lives (l) and health (h) to be set.
	 */
	Player(EventHandler& handler, int h, int l);

	/*
	 setSpawnPoint(double, double)
	 Sets the spawn point for this Player to the specified x and y.
	 */
	inline void setSpawnPoint(double x, double y) {

		spawnPos.x = x;
		spawnPos.y = y;
	}

	/*
	 setHealth(char h)
	 Sets the current health of the player.
	 */
	inline void setHealth(char h) { if (h <= maxHealth) health = h; }
	/*
	 modifyHealth(int h)
	 Adjusts the current health of the player.
	 */
	void modifyHealth(char h);
	/*
	 unsigned char getHealth() const
	 Gets the current health of the player.
	 */
	inline unsigned char getHealth() const { return health; }
	/*
	 setMexHealth(unsigned char)
	 Sets the upper limit of player health.
	 */
	inline void setMaxHealth(unsigned char h) { maxHealth = h; }
	/*
	 setLives(unsigned char)
	 Sets the number of lives the player has.
	 */
	inline void setLives(unsigned char l) { lives = l; }
	/*
	 modifyLives(char)
	 Adjusts the number of lives the player has by the provided amount.
	 */
	inline void modifyLives(char l) { lives += l; }
	/*
	 unsigned char getLives() const
	 Gets the number of lives the player has left.
	 */
	inline unsigned char getLives() const { return lives; }
	/*
	 setPoints(int)
	 Sets the number of points the player has.
	 */
	inline void setPoints(int p) { totalPoints = p; }
	/*
	 modifyPoints(int)
	 Adjusts the number of points the player has.
	 */
	inline void modifyPoints(int p) { totalPoints += p; }
	/*
	 unsigned int getPoints() const
	 Gets the number of points the player currently has.
	 */
	inline unsigned int getPoints() const { return totalPoints; }

	/*
	 startInvuln()
	 Starts invulnerability on the player.
	 */
	inline void startInvuln() {
		
		invuln = true; 
		invulnStart = clock();
	}
	/*
	 bool isInvuln() const
	 Returns true if the player is invulnerable. False otherwise.
	 */
	inline bool isInvuln() const { return invuln; }

	/*
	 kill()
	 Kills the player, docking a life a respawning them.
	 */
	inline void kill() {

		if (lives > 0) {
			lives--;
			health = maxHealth;
			respawn();
		} else {
			ev.setGameState(GAME_OVER);
		}
	}

	/*
	 respawn()
	 Resets this player to their last checkpointed spawn point.
	 */
	void respawn();

	/*
	 setTexture(GLuint, int, int)
	 Sets the texture for the player.
	 */
	void setTexture(GLuint t, int w, int h);

	/*
	 Updates the verticies for this Player.
	 */
	void updateVerts();

	/*
	 update()
	 Update the player logic.
	 */
	void update(double deltaT);
	/*
	 handleInput()
	 Deal with user input using the EventHandler we passed in the constructor.
	 */
	void handleInput(double deltaT);
	/*
	 bool canJup()
	 Checks if the player is able to jump or not.
	 Returns true if the player can jump, false otherwise.
	 */
	bool canJump();
	/*
	 draw()
	 Draws the Player to the screen.
	 */
	void draw();

	inline void collide(StaticEntity& ent, Vertex& mtd) { ent.respond(*this, mtd); }

	/*
	 Collision handlers.
	 */
	void respond(Platform& ent, Vertex& mtd);
	void respond(MovingPlatform& ent, Vertex& mtd);
	void respond(WalkingEnemy& ent, Vertex& mtd);
	void respond(JumpingEnemy& ent, Vertex& mtd);
	void respond(GameGoal& ent, Vertex& mtd);
	
};

/*
 ostream& operator<<(ostream&, Player&)
 Allows stream output of basic information about the right hand Player object.
 */
std::ostream& operator<<(std::ostream& out, const Player& p);
/*
 istream& operator>>(wistream&, Player&)
 Allows stream input to set the Player x and y pos, health and lives.
 */
std::wistream& operator>>(std::wistream& in, Player& p);

#endif